import io.reactivex.rxjava3.core.*;
import io.reactivex.rxjava3.subscribers.TestSubscriber;

import java.nio.charset.StandardCharsets;

public class RxMain {
    public static void main(String[] args) {

        Observable<byte[]> observableByteStream = StringObservable.from(inputStream);
        Observable<String> stringObservable = StringObservable.decode(observableByteStream,
                StandardCharsets.UTF_8);
        Observable<String> splittedObservable = StringObservable.split
        stringObservable.subscribe(new TestSubscriber<>());

        Flowable.just("Hello world").subscribe(System.out::println);
    }
}
