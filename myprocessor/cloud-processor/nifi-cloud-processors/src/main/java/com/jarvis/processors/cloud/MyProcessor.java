/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jarvis.processors.cloud;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.io.OutputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.io.IOUtils;

import rx.Observable;

import rx.Subscriber;
import rx.observables.GroupedObservable;
import rx.observables.StringObservable;
import rx.observers.TestSubscriber;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;

@Tags({"example "})
@CapabilityDescription("Provide a description")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="", description="")})
public class MyProcessor extends AbstractProcessor {

    public static final PropertyDescriptor MY_PROPERTY = new PropertyDescriptor
            .Builder().name("MY_PROPERTY")
            .displayName("My property")
            .description("Example Property")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    public static final Relationship MY_RELATIONSHIP = new Relationship.Builder()
            .name("MY_RELATIONSHIP")
            .description("Example relationship")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(MY_PROPERTY);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(MY_RELATIONSHIP);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }

        // TODO implement
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        int[] numOutRecords = {0};

//        final AtomicReference<String> value = new AtomicReference<>();
        session.read(flowFile, new InputStreamCallback() {
            @Override
            public void process(InputStream inputStream) throws IOException {
                try {
                Object[] lockObj = {new Object()};
                System.setProperty("rx.ring-buffer.size", "150000");
                Kryo kryo = new Kryo();
                kryo.register(PingMeshKryo.class);
                kryo.register(Integer.class);
                kryo.register(SrcClusterStatsKryo.class);
                Output output = new Output(outputStream);

                PublishSubject<PingMeshKryo> subject = PublishSubject.create();
                subject.
                        observeOn(Schedulers.computation()).
                        filter(v -> v.m_errcode==1).
                        groupBy(v -> v.m_src_cluster).
                        flatMap(group -> {
                            return group.toList().map(grpList -> {
//                                  System.out.println("Groups are " + group.getKey());
                                int max = 0, min = Integer.MAX_VALUE, count=0;
                                float avg = 0;
                                for (PingMeshKryo data :
                                        grpList) {
                                    if(data.m_rtt > max) {
                                        max = data.m_rtt;
                                    }

                                    if(data.m_rtt < min) {
                                        min = data.m_rtt;
                                    }

                                    avg += data.m_rtt;
                                    count++;
                                }

                                SrcClusterStatsKryo srcClusterStatsKryo = new SrcClusterStatsKryo();
                                srcClusterStatsKryo.m_src_cluster = group.getKey();
                                srcClusterStatsKryo.avgRtt = (float)avg/(float)count;
                                srcClusterStatsKryo.minRtt = min;
                                srcClusterStatsKryo.maxRtt = max;
                                return srcClusterStatsKryo;
                            });
                        }).
//                        subscribe(s ->
//                        {
//                            kryo.writeObject(output, s);
//                            numOutRecords[0]++;
////                        recordsListFilter.add(s);
//                        });
                        subscribe(new Subscriber<SrcClusterStatsKryo>() {
                            @Override
                            public void onCompleted() {
                                synchronized(lockObj[0]) {
                                    lockObj[0].notify();
                                }
                                System.out.println("On complete called in subscriber");
                            }

                            @Override
                            public void onError(Throwable throwable) {

                            }

                            @Override
                            public void onNext(SrcClusterStatsKryo srcClusterStatsKryo) {
                                try {
                                    kryo.writeObject(output, srcClusterStatsKryo);
                                } catch (Exception e) {
                                    System.err.println("Couldn't write to output stream : " + e.toString());
                                }
                            }
                        });

                // Reading the data
                Input input = new Input(inputStream);
                Integer numRecords = (Integer) kryo.readObject(input, Integer.class);
                PingMeshKryo object2 = kryo.readObject(input, PingMeshKryo.class);
                subject.onNext(object2);
                int read = 1;
                while(read < numRecords) {
                    object2 = kryo.readObject(input, PingMeshKryo.class);
                    subject.onNext(object2);
                    read++;
                }
                subject.onCompleted();

                // Wait for query dataflow to complete
                synchronized (lockObj[0]) {
                    lockObj[0].wait();
                }

                output.close();
                input.close();

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("File not found exception");
            }

        }
        });

        // To write the results back out ot flow file
        flowFile = session.write(flowFile, new OutputStreamCallback() {

            @Override
            public void process(OutputStream out) throws IOException {
                out.write(outputStream.toByteArray());
            }
        });

        session.transfer(flowFile,MY_RELATIONSHIP);
    }
}