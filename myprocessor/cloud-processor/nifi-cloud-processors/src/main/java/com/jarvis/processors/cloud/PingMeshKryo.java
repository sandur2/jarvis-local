package com.jarvis.processors.cloud;

public class PingMeshKryo {
    String m_constantCol;
    String m_event_time;
    int m_src_cluster;
    String m_src_ip;
    String m_src_port;
    String m_dest_cluster;
    String m_dest_ip;
    String m_dest_port;
    String m_seq_num;
    int m_rtt;
    int m_errcode;
    String m_result_type;
    String m_level;
    String m_msglen;
}
