/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.sun.org.apache.xalan.internal.res.XSLTErrorResources_en;
import org.apache.nifi.util.MockFlowFile;
import org.apache.nifi.util.TestRunner;
import org.apache.nifi.util.TestRunners;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.util.List;

import static com.jarvis.processors.edge.MyProcessor.MY_RELATIONSHIP;
import static com.jarvis.processors.edge.MyProcessor.MY_RELATIONSHIP_1;


public class MyProcessorTest {

    private TestRunner testRunner;

    @Before
    public void init() {
        testRunner = TestRunners.newTestRunner(MyProcessor.class);
    }

    @Test
    public void testProcessor() {

        File flowfileInp = new File("/home/athuls89/Desktop/OSL/msr/Datasets/PingMesh/xxa-1000records.allintify.kryo");
        try {
            InputStream in = null;

            int flowFileCount = 0;
            int shortSleep = 500;
            int mediumSleep = 1500;
            int longSleep = 3000;
            while(flowFileCount < 20) {
                in = new FileInputStream(flowfileInp);
                testRunner.enqueue(in);
                testRunner.run(1);
                if(flowFileCount <= 5) {
                    Thread.sleep(longSleep);
                } else if(flowFileCount < 20) {
                    Thread.sleep(shortSleep);
                } else {
                    Thread.sleep(mediumSleep);
                }

                testRunner.assertQueueEmpty();
                flowFileCount++;
            }

            Thread.sleep(10000);

            List<MockFlowFile> results = testRunner.getFlowFilesForRelationship(MY_RELATIONSHIP);
            System.out.println("Size of MY_RELATIONSHIP is " + results.size());

            List<MockFlowFile> results1 = testRunner.getFlowFilesForRelationship(MY_RELATIONSHIP_1);
            System.out.println("Size of MY_RELATIONSHIP_1 is " + results1.size());

            Kryo m_kryo = new Kryo();
            m_kryo.register(PingMeshKryo.class);
            byte[] outputEdge = testRunner.getContentAsByteArray(results1.get(0));
            ByteArrayInputStream outputEdgeStream = new ByteArrayInputStream(outputEdge);
            Input kryoInput = new Input(outputEdgeStream);
            PingMeshKryo object2 = m_kryo.readObject(kryoInput, PingMeshKryo.class);
            int read = 1;
            try {
                while (object2 != null) {
                    object2 = m_kryo.readObject(kryoInput, PingMeshKryo.class);
                    read++;
                }
            } catch (Exception ex) {
                System.out.println("Done reading");
            }

            System.out.println("Edge processing record count: " + read);
        } catch (Exception ex) {
            System.out.println("Couldn't read flowfile input " + ex.toString());
            ex.printStackTrace();
        }
//        PingMeshKryo dummyWaterMark = new PingMeshKryo();
//        dummyWaterMark.m_constantCol= Integer.MIN_VALUE;
//        PingMeshKryoWithTime dummyWaterMarkWithTime = new PingMeshKryoWithTime();
//        dummyWaterMarkWithTime.setEntity(dummyWaterMark);
//
//        Kryo m_kryo = new Kryo();
//        m_kryo.register(PingMeshKryo.class);
//        m_kryo.register(PingMeshKryoWithTime.class);
//        m_kryo.register(Integer.class);
//        m_kryo.register(SrcClusterStatsKryo.class);
//        String queueThresholdConfig = "15000000";
//        JarvisLogger.info("[Atul] Queue threshold config is " + queueThresholdConfig);
//        SyncQueueWrapper m_operatorQueue = new SyncQueueWrapper<>(1, Integer.parseInt(queueThresholdConfig), m_kryo,
//                MY_RELATIONSHIP_1, dummyWaterMarkWithTime);
////        m_operatorQueue.initializeKryo(new PingMeshKryoWithTime(), new PingMeshKryo());
//        OutputQueueWrapper m_exitQueue = new OutputQueueWrapper<>(m_kryo, MY_RELATIONSHIP);
//
//        // Start runtime in the end
//        Runtime jarvisRuntime = new Runtime(m_exitQueue, m_operatorQueue);
//        Thread runtimeThread = new Thread(jarvisRuntime);
//        runtimeThread.start();
    }

}
