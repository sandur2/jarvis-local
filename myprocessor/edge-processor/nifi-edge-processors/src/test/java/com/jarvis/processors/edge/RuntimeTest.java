package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import org.junit.Before;
import org.junit.Test;

import static com.jarvis.processors.edge.MyProcessor.MY_RELATIONSHIP;
import static com.jarvis.processors.edge.MyProcessor.MY_RELATIONSHIP_1;
import static org.junit.Assert.*;

public class RuntimeTest {

    @Test
    public void test1() throws Exception {
        PingMeshKryo dummyWaterMark = new PingMeshKryo();
        dummyWaterMark.m_constantCol= Integer.MIN_VALUE;
        PingMeshKryoWithTime dummyWaterMarkWithTime = new PingMeshKryoWithTime();
        dummyWaterMarkWithTime.setEntity(dummyWaterMark);

        Kryo m_kryo = new Kryo();
        m_kryo.register(PingMeshKryo.class);
        m_kryo.register(PingMeshKryoWithTime.class);
        m_kryo.register(Integer.class);
        m_kryo.register(SrcClusterStatsKryo.class);
        String queueThresholdConfig = "15000000";
        ControllerQueue m_operatorQueue = new ControllerQueue(1, Integer.parseInt(queueThresholdConfig), m_kryo,
                MY_RELATIONSHIP_1, dummyWaterMarkWithTime);
//        m_operatorQueue.initializeKryo(new PingMeshKryoWithTime(), new PingMeshKryo());
        OutputQueueWrapper m_exitQueue = new OutputQueueWrapper(m_kryo, MY_RELATIONSHIP);

        // Start runtime in the end
        Runtime jarvisRuntime = new Runtime(m_exitQueue, m_operatorQueue);
        Thread runtimeThread = new Thread(jarvisRuntime);
        runtimeThread.start();
    }
}