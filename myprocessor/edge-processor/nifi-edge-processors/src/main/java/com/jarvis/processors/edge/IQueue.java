package com.jarvis.processors.edge;

public interface IQueue {
    void put(IData item);
    void putWaterMark(IData item);
    byte[] getByteArrayOutputStreamContent();
    void reset();
    void waitForWatermarkWithSeqNum(int seqNum);
    int waitForNewEpochAndGetSeqNum();
    void unseeWatermark();
    long getRecentIdleTime();
    int getRecentRecordsDrainedSize();
    double getRecentPayloadSizeOnEdge();
    int getRecentRecordsDiscardedSize();
    long getRecentEpochTime();
    void enableSendingToEdge();
    void setProbSendingToEdge(double probSendingToEdge);
    void enableSendingToEdgeFlagOnly();
    void disableSendingToEdge();
}
