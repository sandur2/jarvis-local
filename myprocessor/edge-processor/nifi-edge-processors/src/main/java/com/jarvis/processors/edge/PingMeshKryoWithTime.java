package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;

public class PingMeshKryoWithTime implements  IData {
    private PingMeshKryo m_data;
    private long timeQueued;

//    public PingMeshKryoWithTime(PingMeshKryo data) {
//        m_data = data;
//        timeQueued = System.currentTimeMillis();
//    }

    public void setTimeQueued() {
        timeQueued = System.currentTimeMillis();
    }

    public int getSeqNum() {
        return getEntity().getSeqNum();
    }

    public void setSeqNum(int seqNum) {
        getEntity().setSeqNum(seqNum);
    }

    public void setEntity(IData data) {
        m_data = (PingMeshKryo) data;
        resetQueueTime();
    }

    public IData getEntity() {
        return m_data;
    }

    public int getPayloadInBytes() {
        return getEntity().getPayloadInBytes();
    }

    public long getQueueTime() {
        return (System.currentTimeMillis() - timeQueued);
    }

    public void resetQueueTime() {
        timeQueued = System.currentTimeMillis();
    }

    public boolean isWaterMark() {
        return this.getEntity().isWaterMark();
    }

    public void writeSelfToKryo(Kryo kryo, Output output) {
        kryo.writeObject(output, (PingMeshKryo) this.getEntity());
    }
}