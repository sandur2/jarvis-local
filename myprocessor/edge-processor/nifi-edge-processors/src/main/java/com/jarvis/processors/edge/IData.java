package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;

public interface IData {
    int getPayloadInBytes();
    long getQueueTime();
    IData getEntity();
    void setEntity(IData data);
    boolean isWaterMark();
    void writeSelfToKryo(Kryo kryo, Output output);
    void resetQueueTime();
    int getSeqNum();
    void setSeqNum(int seqNum);
}
