package com.jarvis.processors.edge;

public enum CongestionState {
    CALIBRATE,
    UNCALIBRATE,
    CONGESTED,
    CLEAR,
    STABLE,
    DATA_INC,
    DATA_DEC,
    OP_INC,
    OP_DEC
}
