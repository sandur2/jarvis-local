package com.jarvis.processors.edge;

import com.jarvis.processors.edge.old.GrpedObsWithTime;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.apache.nifi.logging.ComponentLog;

import java.util.concurrent.LinkedBlockingQueue;

public class CustomGroupingOperator implements Runnable {
    LinkedBlockingQueue<PingMeshKryoWithTime> m_currentQueue;
    LinkedBlockingQueue<GrpedObsWithTime> m_nextQueue;
    io.reactivex.subjects.PublishSubject<PingMeshKryo> m_subject = io.reactivex.subjects.PublishSubject.create();
    long totalProcessingTime = 0;
    ComponentLog m_logger;
    long m_maxQueueTime = 0;
    float m_avgProcessingTime = 0;
    long m_dataflowBuildDur = 0;

    public CustomGroupingOperator(LinkedBlockingQueue<PingMeshKryoWithTime> currentQueue, ComponentLog logger) {
        m_currentQueue = currentQueue;
        m_logger = logger;
        Long[] processStart = {0L};
        Long[] processDur = {0L};
        Long startDataflowBuild = System.currentTimeMillis();

        m_subject.
                doOnNext(v -> {
                    processStart[0] = System.currentTimeMillis();
                }).
                groupBy(v -> v.m_src_cluster).
                doOnNext(v -> {
//                    processDur[0] = System.currentTimeMillis() - processStart[0];
//                    System.out.println("Right after groupby: " + processDur[0]);
                    totalProcessingTime += (System.currentTimeMillis() - processStart[0]);
                }).subscribe(
                new Observer<io.reactivex.observables.GroupedObservable<Integer, PingMeshKryo>>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onComplete() {
//                                m_logger.debug("On complete called in subscriber with current queue size:" +
//                                        m_currentQueue.size() + " and next: " + m_nextQueue.size());
                        GrpedObsWithTime waterMarkEntry = new GrpedObsWithTime(null);
                        waterMarkEntry.m_watermark = true;
                        m_nextQueue.offer(waterMarkEntry);
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(io.reactivex.observables.GroupedObservable<Integer, PingMeshKryo> groupedObservable) {
                        try {
                            m_nextQueue.offer(new GrpedObsWithTime(groupedObservable));
                        } catch (Exception e) {
                            m_logger.debug("Couldn't write to output stream : " + e.toString());
                        }
                    }
                }
        );;

//        m_logger.debug("Grping Dataflow build duration is " + (System.currentTimeMillis() - startDataflowBuild));
        m_dataflowBuildDur=(System.currentTimeMillis() - startDataflowBuild);
    }


    public void setNextQueue(LinkedBlockingQueue<GrpedObsWithTime> nextQueue) {
        m_nextQueue = nextQueue;
    }

    public void setCurrentQueue(LinkedBlockingQueue<PingMeshKryoWithTime> currentQueue) {
        m_currentQueue = currentQueue;
    }

    public void setSubject(io.reactivex.subjects.PublishSubject<PingMeshKryo> subject) {
        m_subject = subject;
    }

    public void setObserver(io.reactivex.subjects.PublishSubject<PingMeshKryoWithTime> subject) {
//        m_dataflow = io.reactivex.Observable.just(m_currentQueue).flatMapIterable(elements -> elements).

//                io.reactivex.Observable.fromIterable(m_currentQueue).
//                    doOnNext(System.out::println).
//                    filter(v -> v.m_errcode==1);
//        m_dataflow = m_subject.doOnNext(m_currentQueue::remove);
//        m_dataflow = subject.filter(v -> v.m_errcode==1);
    }

    public void run() {
//        m_dataflow = Observable.just(m_currentQueue.poll()).
//                observe().

        Long[] flapMapDur = {0L};

        PingMeshKryoWithTime readObj;
        long totalQueueTime = 0;
        int recordCount = 0;
        try {
//            m_logger.debug("Dequeue start grouping: " + System.currentTimeMillis());
            while ((readObj = m_currentQueue.take()) != null) {
                if(readObj.isWaterMark()) {
                    m_subject.onComplete();
                    break;
                }
//                System.out.println("Record");
                totalQueueTime = readObj.getQueueTime();
                m_subject.onNext((PingMeshKryo) readObj.getEntity());
                recordCount++;
//                System.out.println("[Grping] Queue size is " + m_currentQueue.size()
//                    + " and queue time is " + totalQueueTime);
                if(m_maxQueueTime<totalQueueTime) {
                    m_maxQueueTime = totalQueueTime;
                }
//                if(recordCount == m_flowFileSize) {
//                    m_subject.onComplete();
//                    break;
//                }
            }

//            m_logger.debug("Grouping max queue time = " + m_maxQueueTime);
//            m_logger.debug("Grouping avg processing time in ns = " + (totalProcessingTime/recordCount));
            m_avgProcessingTime=(float)totalProcessingTime/(float)recordCount;
        } catch (Exception ex) {
            m_logger.debug("Exception waiting: " + ex.toString());
        }
    }
}

