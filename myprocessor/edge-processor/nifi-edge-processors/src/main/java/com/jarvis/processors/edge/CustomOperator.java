package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.apache.nifi.logging.ComponentLog;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class CustomOperator implements Runnable{
    ControllerQueue m_currentQueue;
    IQueue m_nextQueue;

    io.reactivex.subjects.PublishSubject<IData> m_subject;
    long totalProcessingTime = 0;
    long m_maxQueueTime;
    float m_avgProcessingTime;
    long m_dataflowBuildDur;
    Integer[] m_numOutRecords = {0};
    int m_opId;
    Random randGen = new Random();
    double m_reductionRatio = 0;
    AtomicInteger m_waterMarkSeqNum = new AtomicInteger(0);

    public CustomOperator(int opId, ControllerQueue currentQueue, double reductionRatio) {
        m_opId = opId;
        m_currentQueue = currentQueue;
        m_subject = io.reactivex.subjects.PublishSubject.create();
        m_reductionRatio = reductionRatio;
    }

    public abstract void setDataflow();
    public abstract void setNextQueue(IQueue queue);

    public Integer[] getOutputRecCount() {
        return m_numOutRecords;
    }

    protected void resetMetrics() {
        m_maxQueueTime = 0;
        m_avgProcessingTime = 0;
        m_dataflowBuildDur = 0;
        m_numOutRecords[0] = 0;
        totalProcessingTime = 0;
    }

    public void run() {

        while(true) {
            try {
//            m_logger.debug("Dequeue start agg: " + System.currentTimeMillis());
                IData readObj;
                boolean watermarkFound = false;
                long totalQueueTime = 0;
                int recordCount = 0;
                resetMetrics();
                setDataflow();
                while (!watermarkFound) {
                    readObj = m_currentQueue.take();
                    if (readObj != null) {
                        if(readObj.isWaterMark()) {
                            m_subject.onComplete();
                            watermarkFound = true;
                        } else {
                            totalQueueTime = readObj.getQueueTime();
                            if(randGen.nextDouble() < m_reductionRatio) {
                                m_subject.onNext(readObj);
                                recordCount++;
                            }

                            if (m_maxQueueTime < totalQueueTime) {
                                m_maxQueueTime = totalQueueTime;
                            }
                        }
                    }
                }

                m_avgProcessingTime = (float) totalProcessingTime / (float) recordCount;
                JarvisLogger.debug("[CustomOperator.run] Records sent from map op " + m_opId + " = "
                        + recordCount + " and written to exit queue: " + m_numOutRecords[0]);

            } catch (Exception ex) {
                JarvisLogger.debug("[CustomOperator.run] Exception waiting: " + ex.toString());
                ex.printStackTrace();
            }
        }
    }
}
