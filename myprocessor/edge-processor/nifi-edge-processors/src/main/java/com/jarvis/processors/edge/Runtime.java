package com.jarvis.processors.edge;

import java.util.ArrayList;
import java.util.List;

public class Runtime implements Runnable {
    public static final int SIZE_OF_INT = 4;
    public static final double IDLE_TIME_DELTA_RATIO_THRESHOLD = 0.2;
    public static final double IDLE_TO_EPOCH_TIME_RATIO_THRESHOLD = 0.1;
    public static final double DRAINED_RECORDS_FRACTION_THRESHOLD = 0.05;
    IQueue[] m_controlProxies;
//    OutputQueueWrapper m_outputControlProxy;
    List<Integer> m_subQueries;
    int m_prevClearSubQueryIdx;
    int m_currentFullOpsSubQueryIdx;
    long m_prevIdleTimes = 0;
    long m_currIdleTimes = 0;
    Integer[] cpDrainedDataSizes;
    Integer[] cpEdgeDataSizes;
    Integer[] cpDiscardedDataSizes;
    int queueId;
    int m_leftSubquery = 0, m_rightSubquery = 0;
//    private boolean calibrationDone = false;
    RuntimeAction m_prevRuntimeAction;
    RuntimeState m_currentRuntimeState;
    boolean finalCpIsSubquery = false;

    double m_probUpdateDelta = 0.1;
    OperatorProbRange[] m_opProbRanges;
    CongestionState prevCongestionState;

    private int numStables = -1;
    private boolean firstTimeStable = true;

    private int m_finalQueueIdx;

    public Runtime(OutputQueueWrapper outputQueue, ControllerQueue ...controlProxies) {
        List<IQueue> controlProxiesList = new ArrayList<>();
        for(ControllerQueue controlProxy : controlProxies) {
            controlProxiesList.add(controlProxy);
        }

        controlProxiesList.add(outputQueue);

        int numControlProxies = controlProxiesList.size();
        m_finalQueueIdx=numControlProxies-1;
        cpDrainedDataSizes = new Integer[numControlProxies];
        cpEdgeDataSizes = new Integer[numControlProxies];
        cpDiscardedDataSizes = new Integer[numControlProxies];
        m_controlProxies = new IQueue[numControlProxies];
        m_controlProxies = controlProxiesList.toArray(m_controlProxies);
//        m_outputControlProxy = outputQueue;
        m_subQueries = new ArrayList<>();
        m_opProbRanges = new OperatorProbRange[numControlProxies-1];
        for (int i = 0; i < numControlProxies-1; i++) {
            m_opProbRanges[i] = new OperatorProbRange();
        }

        m_prevRuntimeAction = RuntimeAction.NO_ACTION;
        prevCongestionState = CongestionState.STABLE;
        m_currentRuntimeState = RuntimeState.STABLE;
        m_prevClearSubQueryIdx = -1;

    }

//    public void calibrate() {
//        for (int i = 0; i < m_internalControlProxies.length; i++) {
//            m_internalControlProxies[i].blockDraining();
//        }
//
//        calibrationDone = true;
//    }

//    public void unCalibrate() {
//        for (int i = 0; i < m_internalControlProxies.length; i++) {
//            m_internalControlProxies[i].unblockDraining();
//        }
//    }

    public void run() {
        int newEpochNum = -1;
        CongestionState currentCongestionState;
        boolean configChanged = false;
        while(true) {
//            m_controlProxies[m_finalQueueIdx].waitForWatermarkWithSeqNum(newEpochNum);
            m_controlProxies[m_finalQueueIdx].waitForWatermarkWithSeqNum(++newEpochNum);
            currentCongestionState = observeControllerQueues();
            switch (currentCongestionState) {
                case CONGESTED:
                    // Need to reduce compute load
                    JarvisLogger.debug("[Runtime.run] Congested state");
                    configChanged = false;
                    if (m_currentRuntimeState == RuntimeState.STABLE && (numStables >= 2 || firstTimeStable)) {
                        calibrateQuery();
                        numStables = 0;
                        m_currentRuntimeState = RuntimeState.ADAPT;
                        firstTimeStable = false;
                    }

                    if((m_prevRuntimeAction==RuntimeAction.OP_INC || m_prevRuntimeAction==RuntimeAction.OP_DEC ||
                            prevCongestionState==CongestionState.STABLE) && reduceOperatorCongestion()){
                        m_prevRuntimeAction = RuntimeAction.OP_DEC;
                        configChanged = true;
                    } else {
                        if(!(m_prevRuntimeAction==RuntimeAction.DATA_INC || m_prevRuntimeAction==RuntimeAction.DATA_DEC)
                                && m_currentFullOpsSubQueryIdx > 0) {
                            m_currentFullOpsSubQueryIdx--;
                        }

                        if(reduceDataCongestion()) {
                            m_prevRuntimeAction=RuntimeAction.DATA_DEC;
                            configChanged=true;
                        }
                    }

//                    if (m_prevRuntimeAction == RuntimeAction.DATA_INC && reduceDataCongestion()) {
//                        m_prevRuntimeAction = RuntimeAction.DATA_DEC;
//                        configChanged = true;
//                    } else if (reduceOperatorCongestion()) {
//                        m_prevRuntimeAction = RuntimeAction.OP_DEC;
//                        configChanged = true;
//                    }

                    if(configChanged) {
//                        newEpochNum = m_controlProxies[0].waitForNewEpochAndGetSeqNum();
//                        newEpochNum++;
                    }

                    break;
                case CLEAR:
                    // Need to increase compute load
                    JarvisLogger.debug("[Runtime.run] Clear state");
                    m_prevClearSubQueryIdx = m_currentFullOpsSubQueryIdx;
                    if (m_currentRuntimeState == RuntimeState.STABLE && (numStables >= 2 || firstTimeStable)) {
                        calibrateQuery();
                        numStables = 0;
                        m_currentRuntimeState = RuntimeState.ADAPT;
                        firstTimeStable = false;
                    }

                    configChanged = false;

                    if((m_prevRuntimeAction==RuntimeAction.OP_INC || m_prevRuntimeAction==RuntimeAction.OP_DEC ||
                        prevCongestionState==CongestionState.STABLE) && increaseOperatorLoad()){
                        m_prevRuntimeAction=RuntimeAction.OP_INC;
                        configChanged=true;
                    } else if(increaseDataLoad()) {
                        m_prevRuntimeAction=RuntimeAction.DATA_INC;
                        configChanged=true;
                    }

//                    if (!(m_prevRuntimeAction == RuntimeAction.NO_ACTION ||
//                            m_prevRuntimeAction == RuntimeAction.OP_INC) && increaseDataLoad()) {
//                        m_prevRuntimeAction = RuntimeAction.DATA_INC;
//                        configChanged = true;
//                    } else if (increaseOperatorLoad()) {
//                        configChanged = true;
//                        m_prevRuntimeAction = RuntimeAction.OP_INC;
//                    }

                    if (!configChanged) {
                        m_prevIdleTimes = m_currIdleTimes;
                        m_prevRuntimeAction = RuntimeAction.NO_ACTION;
                        m_currentRuntimeState = RuntimeState.STABLE;
                    } else {
//                        newEpochNum = m_controlProxies[0].waitForNewEpochAndGetSeqNum();
//                        newEpochNum++;
                    }

                    break;
                case STABLE:
                    JarvisLogger.debug("[Runtime.run] Stable state");
                    numStables++;
                    if(numStables >= 2 || firstTimeStable) {
                        m_prevRuntimeAction = RuntimeAction.NO_ACTION;
                        m_currentRuntimeState = RuntimeState.STABLE;
                        m_prevIdleTimes = m_currIdleTimes;
//                  //  newEpochNum = -1;
                    }

                    break;
            }

            prevCongestionState = currentCongestionState;
            m_controlProxies[m_finalQueueIdx].unseeWatermark();
        }
    }

    public CongestionState observeControllerQueues() {
        CongestionState state = CongestionState.STABLE;
        double congestedRecordCount = 0;
        long totalIdleTime = 0;
        double avgIdleTimeRatio = 0;
        int id;

        // Only iterate over internal control proxies
        for (id = 0; id < m_finalQueueIdx; id++) {
            IQueue queue = m_controlProxies[id];

            // Detect clear state
            long currIdleTime = queue.getRecentIdleTime();
            cpDrainedDataSizes[id] = queue.getRecentRecordsDrainedSize();
            cpEdgeDataSizes[id] = (int)queue.getRecentPayloadSizeOnEdge();
            cpDiscardedDataSizes[id] = queue.getRecentRecordsDiscardedSize();
            long epochTime = queue.getRecentEpochTime();
            double drainedRecordsSizeRatio =
                    (double) cpDrainedDataSizes[id]/(double) (cpDrainedDataSizes[id] + cpEdgeDataSizes[id]);
            double idleTimeRatio = (double) currIdleTime/(double) epochTime;
            JarvisLogger.debug("[Runtime.observeControllerQueues] Records drained size: " + cpDrainedDataSizes[id] +
                    " and idle time: " + currIdleTime);
            if(cpDrainedDataSizes[id] > 0) {
                // Detect congested state
                // TODO: Do we need threshold?
//                congestedRecordCount += recentRecordsDrained;
                congestedRecordCount += drainedRecordsSizeRatio;
            } else {
                // [OLDER] if(currIdleTime > 0 && currIdleTime > m_prevIdleTimes[queueId])
                // Detect idle time
                // TODO: Do we need threshold?
                totalIdleTime += currIdleTime;
                avgIdleTimeRatio += idleTimeRatio;
            }
        }

        m_currIdleTimes = totalIdleTime;
        double avgRecordsDrainedSize = (double) congestedRecordCount/(double) (id+1);
        avgIdleTimeRatio = avgIdleTimeRatio/(double) (id+1);
        if(avgRecordsDrainedSize > DRAINED_RECORDS_FRACTION_THRESHOLD) {
            state = CongestionState.CONGESTED;
            m_prevIdleTimes = 0; // Idle time is reset
        } else if(congestedRecordCount == 0 && avgIdleTimeRatio > IDLE_TO_EPOCH_TIME_RATIO_THRESHOLD &&
                (totalIdleTime-m_prevIdleTimes) > IDLE_TIME_DELTA_RATIO_THRESHOLD * m_prevIdleTimes){
            // Check first condition as well because previous idle time can
            // be negative since its set to current idle time:
            state = CongestionState.CLEAR;
            // TODO: how to ensure once you enter CLEAR and before RuntimeState goes back to STABLE, we need to stay in CLEAR
//            m_prevIdleTimes = totalIdleTime;
        }

        return state;
    }

    public void calibrateQuery() {
        double origDataSizeEdge =  cpEdgeDataSizes[0];
        int opOutputSize = 0;
        double prevDataRedRatio = 1;

        m_subQueries.clear();
        m_subQueries.add(0);
        finalCpIsSubquery = false;
        for (int i = 1; i < m_finalQueueIdx; i++) {
            opOutputSize = cpEdgeDataSizes[i] + cpDrainedDataSizes[i] + cpDiscardedDataSizes[i];
            double currDataRedRatio = (double) opOutputSize/origDataSizeEdge;
            JarvisLogger.debug("[Runtime.calibrateQuery] O/P size: " + opOutputSize +
                    " orig size: " + origDataSizeEdge + ", discarded size: " + cpDiscardedDataSizes[i] + ", " +
                    "edge data size: " + cpEdgeDataSizes[i]);
            if(currDataRedRatio < prevDataRedRatio) {
                m_subQueries.add(i);
                prevDataRedRatio = currDataRedRatio;
            }
        }

        double outputDataSize = m_controlProxies[m_finalQueueIdx].getRecentPayloadSizeOnEdge();
        double finalDataRedRatio = outputDataSize/origDataSizeEdge;
        JarvisLogger.debug("[Runtime.calibrateQuery] Final O/P size: " + outputDataSize);
        if(finalDataRedRatio < prevDataRedRatio) {
//            m_currentSubQueryIdx = -1;
            m_subQueries.add(m_finalQueueIdx);
            finalCpIsSubquery = true;
        }
//        else {
        // Start with maximum subquery
        m_currentFullOpsSubQueryIdx = m_subQueries.size() - 1;
//        }

        resetSearchIndexes();
        JarvisLogger.debug("[Runtime.calibrateQuery] current subquery index is " +
                m_currentFullOpsSubQueryIdx);
    }

    public void resetSearchIndexes() {
        // Reset indexes for operator binary search
        m_rightSubquery = m_subQueries.size() - 1;
        m_leftSubquery = 0;
        for(int i = 0; i < m_opProbRanges.length; i++) {
            m_opProbRanges[i].reset();
        }

        for(int i = 0; i < m_finalQueueIdx; i++) {
            m_controlProxies[i].enableSendingToEdge();
        }
    }

    public boolean reduceOperatorCongestion() {
        // Operator partitioning binary search
        boolean operatorSearchResult = false;
//        if(m_currentSubQueryIdx == -1) {
//            m_currentSubQueryIdx = m_subQueries.size() - 1;
//            operatorSearchResult = true;
//        } else {
        m_rightSubquery = m_currentFullOpsSubQueryIdx - 1;
        if (m_rightSubquery >= m_leftSubquery) {
            enableProxyCompletely();
            m_currentFullOpsSubQueryIdx = (int) Math.floor((m_rightSubquery + m_leftSubquery) / 2);
            operatorSearchResult = true;
            disableProxyCompletely();

        }
//        }

        JarvisLogger.debug("[Runtime.reduceOperatorCongestion] current subquery idx is: " + m_currentFullOpsSubQueryIdx +
                " , subquery size: "+m_subQueries.size());
        return operatorSearchResult;
    }

    public boolean reduceDataCongestion() {
        boolean probUpdated = false;
        int cpIndexToUpdate = m_currentFullOpsSubQueryIdx; // This was previously m_currentSubQueryIdx-1
        int queueIdx = cpIndexToUpdate >= 0 ? m_subQueries.get(cpIndexToUpdate) : m_finalQueueIdx;
        if(queueIdx != m_finalQueueIdx) {
            double probCurrent = m_opProbRanges[queueIdx].getProbCurrent();
            if(probCurrent > 0) {
                double probHigh = probCurrent - m_probUpdateDelta;
                m_opProbRanges[queueIdx].setProbHigh(probHigh);
                double probLow = m_opProbRanges[queueIdx].getProbLow();
                if (probLow <= probHigh) {
                    probCurrent = (double) (probHigh + probLow) / (double) 2;
                    m_opProbRanges[queueIdx].setProbCurrent(probCurrent);
                    m_controlProxies[queueIdx].setProbSendingToEdge(probCurrent);
                    probUpdated = true;
                }

                //            disableProxyCompletely(m_currentFullOpsSubQueryIdx +1);
                JarvisLogger.debug("[Runtime.reduceDataCongestion] current subquery idx updated is: " + cpIndexToUpdate +
                        " , subquery size: "+m_subQueries.size() + " , probCurrent is: " + probCurrent + ", probHigh is: " + probHigh +
                        " , probLow is: " + probLow);
            } else {
                JarvisLogger.debug("[Runtime.reduceDataCongestion] Prob current is 0, no scope to further reduce data congestion");
            }
        } else {
            JarvisLogger.debug("[Runtime.reduceDataCongestion] current subquery idx updated is: " + cpIndexToUpdate +
                    " , subquery size: " + m_subQueries.size());
        }

        return probUpdated;
    }

    public boolean increaseDataLoad() {
        boolean probUpdated = false;
        int queueIdx = m_subQueries.get(m_currentFullOpsSubQueryIdx);
        if(queueIdx != m_finalQueueIdx) {
            double probCurrent = m_opProbRanges[queueIdx].getProbCurrent();
            if(probCurrent < 1) {
                m_controlProxies[queueIdx].enableSendingToEdgeFlagOnly();
                double probLow = probCurrent + m_probUpdateDelta;
                m_opProbRanges[queueIdx].setProbLow(probLow);
                double probHigh = m_opProbRanges[queueIdx].getProbHigh();
                if (probLow <= probHigh) {
                    probCurrent = (double) (probHigh + probLow) / (double) 2;
                    m_opProbRanges[queueIdx].setProbCurrent(probCurrent);
                    m_controlProxies[queueIdx].setProbSendingToEdge(probCurrent);
                    probUpdated = true;
                }

                disableProxyCompletely(m_currentFullOpsSubQueryIdx + 1);
                JarvisLogger.debug("[Runtime.increaseDataLoad] current subquery idx updated is: " + m_currentFullOpsSubQueryIdx +
                        " , subquery size: " + m_subQueries.size() + " , probCurrent is: " + probCurrent + ", probHigh is: " + probHigh +
                        " , probLow is: " + probLow);
            } else {
                JarvisLogger.debug("[Runtime.increaseDataLoad] Prob current is 1, no scope for increasing data load");
            }
        } else {
            JarvisLogger.debug("[Runtime.increaseDataLoad] current subquery idx updated is: " + m_currentFullOpsSubQueryIdx +
                    " , subquery size: " + m_subQueries.size());
        }

        return probUpdated;
    }

    private void enableProxyCompletely() {
        int currIdx = m_subQueries.get(m_currentFullOpsSubQueryIdx);
        if(currIdx!=m_finalQueueIdx) {
            m_controlProxies[currIdx].enableSendingToEdge();
            m_opProbRanges[currIdx].setProbCurrent(1);
            m_opProbRanges[currIdx].setProbHigh(1);
            m_opProbRanges[currIdx].setProbLow(0);
        }
    }

    private void disableProxyCompletely() {
        disableProxyCompletely(m_currentFullOpsSubQueryIdx);
    }

    private void disableProxyCompletely(int idx) {
        int currIdx = idx < m_subQueries.size() ? m_subQueries.get(idx) : m_finalQueueIdx;
        if(currIdx!=m_finalQueueIdx) {
            m_controlProxies[currIdx].disableSendingToEdge();
            m_opProbRanges[currIdx].setProbCurrent(0);
            m_opProbRanges[currIdx].setProbHigh(1);
            m_opProbRanges[currIdx].setProbLow(0);
        }
    }

    public boolean increaseOperatorLoad() {
        boolean operatorSearchResult = false;
//        enableProxyCompletely();
        m_leftSubquery = m_currentFullOpsSubQueryIdx + 1;
//            if (finalCpIsSubquery && m_currentSubQueryIdx == m_subQueries.size() - 1) {
//                m_currentSubQueryIdx = -1;
//                operatorSearchResult = true;
//            } else
        if (m_rightSubquery >= m_leftSubquery) {
            enableProxyCompletely();
            m_currentFullOpsSubQueryIdx = (int) Math.floor((m_rightSubquery + m_leftSubquery) / 2);
            operatorSearchResult = true;
            disableProxyCompletely();
        }
        JarvisLogger.debug("[Runtime.increaseOperatorLoad] current subquery idx is: " + m_currentFullOpsSubQueryIdx +
                " , subquery size: "+m_subQueries.size());
        return operatorSearchResult;
    }

//    public void reduceSubQuery() {
//        if(m_currentSubQueryIdx == -1) {
//            m_currentSubQueryIdx = m_subQueries.size() - 1;
//            m_internalControlProxies[m_subQueries.get(m_currentSubQueryIdx)].setDisableSendingToEdge();
//        } else {
//            if(m_currentSubQueryIdx > 0) {
//                m_currentSubQueryIdx -= 1;
//            }
//
//            m_internalControlProxies[m_subQueries.get(m_currentSubQueryIdx)].setDisableSendingToEdge();
//        }
//    }

//    public void increaseSubQuery() {
//        if(m_currentSubQueryIdx != -1) {
//            if(m_currentSubQueryIdx < m_subQueries.size() - 1) {
//                m_internalControlProxies[m_subQueries.get(m_currentSubQueryIdx)].unsetDisableSendingToEdge();
//                m_currentSubQueryIdx++;
//                m_internalControlProxies[m_subQueries.get(m_currentSubQueryIdx)].setDisableSendingToEdge();
//            } else {
//                m_internalControlProxies[m_subQueries.get(m_currentSubQueryIdx)].unsetDisableSendingToEdge();
//                m_currentSubQueryIdx = -1;
//            }
//        }
//    }

//    public void updateProbSendingToEdge() {
//        int id = 0;
//        for (ControllerQueue queue :
//                m_internalControlProxies) {
//            int numRecordsDrained = cpDrainedDataSizes[id];
//            updateProbSendingToEdge(queue, numRecordsDrained);
//            id++;
//        }
//    }

    public void updateProbSendingToEdge(ControllerQueue queue, int numRecordsDrained) {
        double probSendingToEdge = queue.getProbSendingToEdge();
        if(numRecordsDrained > 1) {
            probSendingToEdge -= m_probUpdateDelta;
        } else {
            probSendingToEdge += m_probUpdateDelta;
        }

        if(probSendingToEdge <= 0) {
            probSendingToEdge = 0;
        } else if(probSendingToEdge >= 1) {
            probSendingToEdge = 1;
        }

        queue.setProbSendingToEdge(probSendingToEdge);
    }
}
