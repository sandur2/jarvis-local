package com.jarvis.processors.edge;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.apache.nifi.logging.ComponentLog;

import java.util.Random;

public class CustomMapOperator extends CustomOperator {

    PingMeshKryoWithTime m_waterMarkEntryWithTime;
    double m_reductionRatio;

    public CustomMapOperator(int opId, ControllerQueue currentQueue, double reductionRatio) {
        super(opId, currentQueue, reductionRatio);
        m_waterMarkEntryWithTime = new PingMeshKryoWithTime();
        PingMeshKryo waterMarkEntry = new PingMeshKryo();
        waterMarkEntry.m_constantCol = Integer.MIN_VALUE;
        m_waterMarkEntryWithTime.setEntity(waterMarkEntry);
    }

    public void setNextQueue(IQueue queue) {
        // No-op since final operator
        m_nextQueue = queue;
    }

    public void setDataflow() {
        Long[] processStart = {0L};
        Long startDataflowBuild = System.currentTimeMillis();
        m_subject = io.reactivex.subjects.PublishSubject.create();
        m_subject.
                doOnNext(v -> {
                    processStart[0] = System.currentTimeMillis();
                }).
                map(v -> {
//                    Thread.sleep(0, 500000000);
                    Thread.sleep(1);
                    return v;
                }).
                doOnNext(v -> {
                    totalProcessingTime += (System.currentTimeMillis() - processStart[0]);
                }).
                subscribe(
                new Observer<IData>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onComplete() {
                        m_waterMarkEntryWithTime.resetQueueTime();
                        m_waterMarkEntryWithTime.setSeqNum(m_waterMarkSeqNum.getAndIncrement());
                        m_nextQueue.putWaterMark(m_waterMarkEntryWithTime);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                    }

                    @Override
                    public void onNext(IData data) {
                        try {
                            data.resetQueueTime();
                            m_nextQueue.put(data);
                            m_numOutRecords[0]++;
                        } catch (Exception e) {
                            JarvisLogger.debug("Couldn't write to output stream : " + e.toString());
                        }
                    }

                }
        );

        m_dataflowBuildDur=(System.currentTimeMillis() - startDataflowBuild);
    }
}

