package com.jarvis.processors.edge;

public class OperatorProbRange {
    double m_probLow;
    double m_probHigh;
    double m_probCurrent;

    OperatorProbRange() {
        m_probHigh = 1;
        m_probLow = 0;
        m_probCurrent = m_probHigh;
    }

    public void reset() {
        m_probHigh = 1;
        m_probLow = 0;
        m_probCurrent = 1;
    }

    public double getProbLow() {
        return m_probLow;
    }

    public double getProbHigh() {
        return m_probHigh;
    }

    public double getProbCurrent() {
        return m_probCurrent;
    }

    public void setProbLow(double probLow) {
        m_probLow = probLow;
    }

    public void setProbHigh(double probHigh) {
        m_probHigh = probHigh;
    }

    public void setProbCurrent(double probCurrent) {
        m_probCurrent = probCurrent;
    }
}
