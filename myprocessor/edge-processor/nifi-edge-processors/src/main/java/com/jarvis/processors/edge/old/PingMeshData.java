package com.jarvis.processors.edge.old;


public class PingMeshData {
    String m_constantCol;
    String m_event_time;
    String m_src_cluster;
    String m_src_ip;
    String m_src_port;
    String m_dest_cluster;
    String m_dest_ip;
    String m_dest_port;
    String m_seq_num;
    int m_rtt;
    String m_errcode;
    String m_result_type;
    String m_level;
    String m_msglen;

    PingMeshData(String[] row) {
        m_constantCol=row[0];
        m_event_time=row[1];
        m_src_cluster=row[2];
        m_src_ip=row[3];
        m_src_port=row[4];
        m_dest_cluster=row[5];
        m_dest_ip=row[6];
        m_dest_port=row[7];
        m_seq_num=row[8];
        m_rtt=Integer.parseInt(row[9]);
        m_errcode=row[10];
        m_result_type=row[11];
        m_level=row[12];
        m_msglen=row[13];
    }


    public String getSrcCluster() {
        return m_src_cluster;
    }

    public int getRtt() {
        return m_rtt;
    }

    public String getErrCode() {
        return m_errcode;
    }

    public String toString() {
        StringBuilder stringPingMesh = new StringBuilder(m_constantCol).append(",");
        stringPingMesh.append(m_event_time).append(",").append(m_src_cluster).append(",").append(m_src_ip).
                append(",").append(m_src_port).append(",").append(m_dest_cluster).append(",").
                append(m_dest_ip).append(",").append(m_dest_port).append(",").append(m_seq_num).append(",").
                append(m_rtt).append(",").append(m_errcode).append(",").append(m_result_type).append(",").
                append(m_level).append(",").append(m_msglen).append("\n");
        return stringPingMesh.toString();
    }

}
