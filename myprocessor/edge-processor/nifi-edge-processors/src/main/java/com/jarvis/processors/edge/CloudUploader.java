package com.jarvis.processors.edge;

import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessSessionFactory;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.io.OutputStreamCallback;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicReference;

public class CloudUploader {

//    IQueue m_queue;
    Relationship m_MYRELATIONSHIP;

    public CloudUploader(Relationship MY_RELATIONSHIP) {
        m_MYRELATIONSHIP = MY_RELATIONSHIP;
    }

    public void sendToCloud(byte[] windowContent) {
        // m_exitQueue.m_output.close();
        // m_operatorQueue.m_networkOutput.close();

        ProcessSession session;
        synchronized (MyProcessor.m_sessionFactoryLock) {
            session = MyProcessor.m_sessionFactory.createSession();
        }

        try {
            // Write out to network flowfiles
            JarvisLogger.debug("[CloudUploader.sendToCloud] The size of network queue is " +
                    windowContent.length);
            if (windowContent.length > 0) {
                final AtomicReference<FlowFile> flowFile = new AtomicReference<>(session.create());
                flowFile.set(session.putAttribute(flowFile.get(),
                        "m_network_type", "op1NetworkFlowfile"));
                flowFile.set(session.write(flowFile.get(), new OutputStreamCallback() {
                    @Override
                    public void process(OutputStream outNetwork) throws IOException {
                        outNetwork.write(windowContent);
                    }
                }));

                session.transfer(flowFile.get(), m_MYRELATIONSHIP);
            }

            session.commit();
        } catch (Exception ex) {
            session.rollback();
        }

//        m_queue.reset();
//        m_operatorQueue.clear();
//        m_exitQueue.clear();
    }
}
