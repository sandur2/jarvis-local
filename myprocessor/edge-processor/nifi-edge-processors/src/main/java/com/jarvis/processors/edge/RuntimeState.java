package com.jarvis.processors.edge;

public enum RuntimeState {
    ADAPT,
    STABLE,
    OBSERVE_CONGESTED,
    OBSERVE_CLEAR,
    OBSERVE_STABLE
}
