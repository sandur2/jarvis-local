///*
// * Licensed to the Apache Software Foundation (ASF) under one or more
// * contributor license agreements.  See the NOTICE file distributed with
// * this work for additional information regarding copyright ownership.
// * The ASF licenses this file to You under the Apache License, Version 2.0
// * (the "License"); you may not use this file except in compliance with
// * the License.  You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package com.jarvis.processors.edge;
//
//import com.esotericsoftware.kryo.Kryo;
//import com.esotericsoftware.kryo.io.Input;
//import com.esotericsoftware.kryo.io.Output;
//import org.apache.nifi.annotation.behavior.ReadsAttribute;
//import org.apache.nifi.annotation.behavior.ReadsAttributes;
//import org.apache.nifi.annotation.behavior.WritesAttribute;
//import org.apache.nifi.annotation.behavior.WritesAttributes;
//import org.apache.nifi.annotation.documentation.CapabilityDescription;
//import org.apache.nifi.annotation.documentation.SeeAlso;
//import org.apache.nifi.annotation.documentation.Tags;
//import org.apache.nifi.annotation.lifecycle.OnScheduled;
//import org.apache.nifi.components.PropertyDescriptor;
//import org.apache.nifi.flowfile.FlowFile;
//import org.apache.nifi.logging.ComponentLog;
//import org.apache.nifi.processor.*;
//import org.apache.nifi.processor.exception.ProcessException;
//import org.apache.nifi.processor.io.InputStreamCallback;
//import org.apache.nifi.processor.io.OutputStreamCallback;
//import org.apache.nifi.processor.util.StandardValidators;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.util.*;
//import java.util.concurrent.LinkedBlockingQueue;
//
//@Tags({"example"})
//@CapabilityDescription("Provide a description")
//@SeeAlso({})
//@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
//@WritesAttributes({@WritesAttribute(attribute="", description="")})
//public class MyProcessorFullGrpby extends AbstractProcessor {
//
//    public static final PropertyDescriptor MY_PROPERTY = new PropertyDescriptor
//            .Builder().name("MY_PROPERTY")
//            .displayName("My property")
//            .description("Example Property")
//            .required(true)
//            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
//            .build();
//
//    public static final Relationship MY_RELATIONSHIP = new Relationship.Builder()
//            .name("MY_RELATIONSHIP")
//            .description("Example relationship")
//            .build();
//
//    private List<PropertyDescriptor> descriptors;
//
//    private Set<Relationship> relationships;
//
//    // Custom Jarvis code
//    private ComponentLog m_logger;
//    CustomFullGroupbyOperator fullGrpbyOp;
//    CustomGroupingOperator groupingOp;
//    CustomAggOperator aggOperator;
//    LinkedBlockingQueue<SrcClusterStatsKryo> resultsQueue;
//    LinkedBlockingQueue<PingMeshKryoWithTime> inputQueue;
//    LinkedBlockingQueue<GrpedObsWithTime> groupingQueue;
//
//    @Override
//    protected void init(final ProcessorInitializationContext context) {
//        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
//        descriptors.add(MY_PROPERTY);
//        this.descriptors = Collections.unmodifiableList(descriptors);
//
//        final Set<Relationship> relationships = new HashSet<Relationship>();
//        relationships.add(MY_RELATIONSHIP);
//        this.relationships = Collections.unmodifiableSet(relationships);
//
//        // Custom Jarvis code
//        this.m_logger=getLogger();
//        resultsQueue = new LinkedBlockingQueue<>();
//        inputQueue = new LinkedBlockingQueue<>();
//        groupingQueue = new LinkedBlockingQueue<>();
//    }
//
//    @Override
//    public Set<Relationship> getRelationships() {
//        return this.relationships;
//    }
//
//    @Override
//    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
//        return descriptors;
//    }
//
//    @OnScheduled
//    public void onScheduled(final ProcessContext context) {
//
//    }
//
//    @Override
//    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
//        FlowFile flowFile = session.get();
//        if ( flowFile == null ) {
//            return;
//        }
//
//        // TODO implement
//
//        // Write current timestamp to get queueing time
//        long queueEndTime = System.currentTimeMillis();
//
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        int[] numOutRecords = {0};
//
//        // Profiling operators
//        long[] startGrpBy = {0};
//        long[] finalGrpBy = {0};
//        long[] startFilter = {0};
//        long[] finalFilter = {0};
//        long[] startExtract = {0};
//        long[] endExtract = {0};
//        Object[] lockObj = {new Object()};
//
//        Output output = new Output(outputStream);
//        Input[] input = new Input[1];
////        Thread[] grpThread = new Thread[1];
////        Thread[] aggThread = new Thread[1];
//        Thread[] fullGrpbyThread = new Thread[1];
//        Kryo kryo = new Kryo();
////        groupingOp = new CustomGroupingOperator(inputQueue, this.m_logger);
////        groupingOp.setNextQueue(groupingQueue);
////        aggOperator = new CustomAggOperator(groupingQueue, this.m_logger);
////        aggOperator.setNextQueue(resultsQueue);
//        fullGrpbyOp = new CustomFullGroupbyOperator(inputQueue, this.m_logger);
//        fullGrpbyOp.setNextQueue(resultsQueue);
//        fullGrpbyOp.setKryoSerializer(kryo);
//        fullGrpbyOp.setOutput(output);
//
////        final AtomicReference<String> value = new AtomicReference<>();
//        session.read(flowFile, new InputStreamCallback() {
//            @Override
//            public void process(InputStream inputStream) throws IOException {
//                try{
////                    String csv = IOUtils.toString(inputStream, "UTF-8");
//                    System.setProperty("rx.ring-buffer.size", "150000");
//
//                    kryo.register(PingMeshKryo.class);
//                    kryo.register(Integer.class);
//                    kryo.register(SrcClusterStatsKryo.class);
//
////                    grpThread[0] = new Thread(groupingOp);
////                    grpThread[0].start();
////
////                    aggThread[0] = new Thread(aggOperator);
////                    aggThread[0].start();
//
//                    fullGrpbyThread[0] = new Thread(fullGrpbyOp);
//                    fullGrpbyThread[0].start();
//
//
//                    PingMeshKryo waterMark = new PingMeshKryo();
//                    waterMark.m_constantCol="watermark";
//                    long start = System.currentTimeMillis();
//
//                    // Reading the data
//                    input[0] = new Input(inputStream);
//                    startExtract[0] = System.currentTimeMillis();
//                    Integer numRecords = (Integer) kryo.readObject(input[0], Integer.class);
//                    PingMeshKryo object2 = kryo.readObject(input[0], PingMeshKryo.class);
//                    inputQueue.put(new PingMeshKryoWithTime(object2));
//                    int read = 1;
//                    while(read < numRecords) {
//                        object2 = kryo.readObject(input[0], PingMeshKryo.class);
//                        inputQueue.put(new PingMeshKryoWithTime(object2));
//                        read++;
//                    }
//
//                    endExtract[0] = System.currentTimeMillis();
//                    inputQueue.put(new PingMeshKryoWithTime(waterMark));
//
//                    // Write output of query so it can be written to flowfile
////                    kryo.writeObject(output, recordsListFilter.size());
////                    for (PingMeshKryo record :
////                            recordsListFilter) {
////                        kryo.writeObject(output, record);
////                    }
//
////                    value.set(csv.toString());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println("File not found exception");
//                }
//
//            }
//        });
//
//        input[0].close();
//
//        try {
////            grpThread[0].join();
////            aggThread[0].join();
//            fullGrpbyThread[0].join();
//        } catch (Exception ex){
//            this.m_logger.debug("[MY DEBUG] Joining on grp and agg threads failed");
//        }
//
//        // Write final query output records to output stream, so it can be written to flowfile
////        kryo.writeObject(output, resultsQueue.size());
////        for (SrcClusterStatsKryo srcClusterStatsKryo :
////                resultsQueue) {
////            kryo.writeObject(output, srcClusterStatsKryo);
////        }
//
//        output.close();
//        long end = System.currentTimeMillis();
//
//        // Write extraction time
//        flowFile = session.putAttribute(flowFile, "m_extractDuration", String.valueOf(endExtract[0]-startExtract[0]));
//
//        // Write groupby time to flowfile attribute
//        flowFile = session.putAttribute(flowFile, "m_queueEndTime", String.valueOf(queueEndTime));
//
////        // Write groupby time to flowfile attribute
////        flowFile = session.putAttribute(flowFile, "m_grpByDuration", String.valueOf(finalGrpBy[0]-startGrpBy[0]));
////
////        // Write filter time to flowfile attribute
////        flowFile = session.putAttribute(flowFile, "m_filterDuration", String.valueOf(finalFilter[0]-startFilter[0]));
//
//        // Write total query time to flowfile attribute
//        flowFile = session.putAttribute(flowFile, "m_totalDuration", String.valueOf(end-queueEndTime));
//
////        // Write record count to flowfile attribute
////        flowFile = session.putAttribute(flowFile, "m_recordCount", String.valueOf(numOutRecords[0]));
////        flowFile = session.putAttribute(flowFile, "m_recordCount", String.valueOf(aggOperator.getOutputRecCount()[0]));
//        flowFile = session.putAttribute(flowFile, "m_recordCount", String.valueOf(fullGrpbyOp.getOutputRecCount()[0]));
//        // Write record count to flowfile attribute
//
//        // Write operator data
//        flowFile = session.putAttribute(flowFile, "m_grpbyop_maxQTime", String.valueOf(fullGrpbyOp.m_maxQueueTime));
//        flowFile = session.putAttribute(flowFile, "m_grpbyop_avgProcTime", String.valueOf(fullGrpbyOp.m_avgProcessingTime));
//        flowFile = session.putAttribute(flowFile, "m_grpbyop_dfbuilddur", String.valueOf(fullGrpbyOp.m_dataflowBuildDur));
////        flowFile = session.putAttribute(flowFile, "m_grpbyop_maxQTime", String.valueOf(groupingOp.m_maxQueueTime));
////        flowFile = session.putAttribute(flowFile, "m_aggop_maxQTime", String.valueOf(aggOperator.m_maxQueueTime));
////        flowFile = session.putAttribute(flowFile, "m_grpbyop_avgProcTime", String.valueOf(groupingOp.m_avgProcessingTime));
////        flowFile = session.putAttribute(flowFile, "m_aggop_avgProcTime", String.valueOf(aggOperator.m_avgProcessingTime));
////        flowFile = session.putAttribute(flowFile, "m_grpbyop_dfbuilddur", String.valueOf(groupingOp.m_dataflowBuildDur));
////        flowFile = session.putAttribute(flowFile, "m_aggop_dfbuilddur", String.valueOf(aggOperator.m_dataflowBuildDur));
//
//
//        // To write the results back out ot flow file
//        flowFile = session.write(flowFile, new OutputStreamCallback() {
//
//            @Override
//            public void process(OutputStream out) throws IOException {
////                out.write(value.get().getBytes());
//                out.write(outputStream.toByteArray());
//            }
//        });
//
//        inputQueue.clear();
//        groupingQueue.clear();
//        resultsQueue.clear();
//
//        session.transfer(flowFile,MY_RELATIONSHIP);
//    }
//}
