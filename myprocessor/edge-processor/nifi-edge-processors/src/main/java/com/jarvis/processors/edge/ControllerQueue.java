package com.jarvis.processors.edge;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.apache.nifi.processor.Relationship;

import java.io.ByteArrayOutputStream;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class ControllerQueue implements IQueue {
    int m_queueId;
    LinkedBlockingQueue<IData> queue = new LinkedBlockingQueue<>();
//    LinkedBlockingQueue<T> m_networkQueue = new LinkedBlockingQueue<>();
    ByteArrayOutputStream m_networkOutputStream;
    Output m_networkOutput;
    Kryo m_kryo;
    int m_threshold;
    CloudUploader m_cloudUploader;
    AtomicInteger waterMarkCountInQueue = new AtomicInteger(0);
    AtomicBoolean drainageTriggered = new AtomicBoolean(false);
    AtomicBoolean lastPutWasWatermark = new AtomicBoolean(false);
    AtomicBoolean m_drainageJustFinished = new AtomicBoolean(false);
    Object watermarkLock = new Object();
    private AtomicLong m_startIdleTimer = new AtomicLong(Long.MAX_VALUE);
    private AtomicLong m_startEpochTimer = new AtomicLong(Long.MAX_VALUE);
    AtomicLong m_recentIdleTimeInMs = new AtomicLong(0);
    AtomicLong m_recentEpochTimeInMs = new AtomicLong(0);

    // Required for observing dataflow state after changing p_e values
    AtomicBoolean m_selfObserve = new AtomicBoolean(false);
    AtomicBoolean m_sampleWmSeqNum = new AtomicBoolean(false);
    Object m_newEpochLock = new Object();
    AtomicInteger m_recentSeqNum = new AtomicInteger(-1);

    // Control proxy variables
    volatile double m_probSendingToEdge = 1;
    AtomicBoolean m_disableSendingToEdge = new AtomicBoolean(false);
    public volatile double m_prevEpochDataSizeSentToEdge = 0;
    private double m_currentEpochDataSizeSentToEdge = 0;
    AtomicInteger m_recentEpochRecordsDrainedSize = new AtomicInteger(0);
    AtomicInteger m_currEpochRecordsDiscardedSize = new AtomicInteger(0);
    AtomicInteger m_prevEpochRecordsDiscardedSize = new AtomicInteger(0);

    Object probSendingToEdgeLock = new Object();

    Random randGen = new Random();

    IData m_waterMarkEntry;

    public ControllerQueue(int queueId, int threshold, Kryo kryo, Relationship MY_RELATIONSHIP, IData watermarkEntry) {
        m_queueId = queueId;
        m_threshold = threshold;
//        m_kryo = new Kryo();
        m_kryo = kryo;
        m_networkOutputStream = new ByteArrayOutputStream();
        m_networkOutput = new Output(m_networkOutputStream);
        m_cloudUploader = new CloudUploader(MY_RELATIONSHIP);
        m_waterMarkEntry = watermarkEntry;
        m_startEpochTimer.set(System.currentTimeMillis());
    }

    public void initializeKryo(Object ...classObj) {
        for (Object classObjInst :
                classObj) {
            m_kryo.register(classObjInst.getClass());
        }
    }

    public boolean isEntryWatermark(IData entry) {
        boolean isWaterMark = false;
        if(entry != null) {
            isWaterMark = entry.isWaterMark();
        }

        return isWaterMark;
    }

    public IData take() {
        IData takenVal = null;
        try{
            takenVal = queue.take();
            if (takenVal.isWaterMark()) {
                synchronized (watermarkLock) {
                    waterMarkCountInQueue.decrementAndGet();
                    drainageTriggered.set(true);
                    if(queue.size() == 0) {
                        m_startIdleTimer.set(System.currentTimeMillis());
                    }

                    JarvisLogger.debug(m_queueId + " [SyncQueue.take] After reading outside of lock, " +
                            "watermark size in take is " + waterMarkCountInQueue.get());

                    // Reset the window size to signify end of current window processing by operator
                    m_prevEpochDataSizeSentToEdge = m_currentEpochDataSizeSentToEdge;
                    m_currentEpochDataSizeSentToEdge = 0;
//                   m_startIdleTimer.compareAndSet(Long.MAX_VALUE, System.currentTimeMillis());
                    JarvisLogger.debug(m_queueId + " [SyncQueue.take] Size of recent window processed is " +
                            m_prevEpochDataSizeSentToEdge);
                    watermarkLock.notify();
                }
            } else {
                m_currentEpochDataSizeSentToEdge += takenVal.getPayloadInBytes();
            }

        } catch (Exception ex) {
            JarvisLogger.debug("[SyncQueue.take] Exception in dequeueuing " + ex.toString());
            ex.printStackTrace();
        }

        return takenVal;
    }

    // TODO: Run with thread pool instead of creating new thread
    // TODO: Evaluate with async drainage too
    private void tryDrainTillWatermark() {
        long start = System.currentTimeMillis();
        IData takenVal;
        int numRecords = 0;
        int recordsDrainedSize = 0;
        boolean watermarkFound = false;

        // Additional condition: && !m_drainBlocked.get()
        if(waterMarkCountInQueue.get() > 0 && !drainageTriggered.get()) {
            synchronized (watermarkLock) {
                // Additional condition: && !m_drainBlocked.get()
                if(waterMarkCountInQueue.get() > 0 && !drainageTriggered.get()) {
                    JarvisLogger.debug(m_queueId + "[tryDrainTillWatermark] Watermark count for take is " +
                            waterMarkCountInQueue.get() + ", Queue size is " + size());
                    while (!watermarkFound && (takenVal = queue.poll()) != null) {
                        // Transfer all queue contents for current window to cloud upload
                        if (isEntryWatermark(takenVal)) {
                            watermarkFound = true;
                            try {
                                queue.put(takenVal);
                            } catch (Exception ex) {
                                JarvisLogger.debug("[SyncQueueWrapper.tryDrainTillWatermark] Couldn't re-insert watermark");
                                ex.printStackTrace();
                            }
//                            m_startIdleTimer.compareAndSet(Long.MAX_VALUE, System.currentTimeMillis());
                        }

                        takenVal.writeSelfToKryo(m_kryo, m_networkOutput);
                        numRecords++;
                        recordsDrainedSize += takenVal.getPayloadInBytes();
                    }

                    // If next op reader dequeued watermark before drainer thread could
                    if(recordsDrainedSize > 0 && !watermarkFound) {
                        m_waterMarkEntry.writeSelfToKryo(m_kryo, m_networkOutput);
                    }

                    JarvisLogger.debug(m_queueId + " [tryDrainTillWatermark] Total records ejected in this drain session is " +
                            numRecords + " and " + "watermark count for take is " + waterMarkCountInQueue.get() +
                            " and queue size is " + size() + ", records drianed size is: " + recordsDrainedSize);
                }

                drainageTriggered.set(true);
            }

            // If there was more than just the watermark
            if(numRecords > 1) {
                byte[] windowContent = getByteArrayOutputStreamContent();
                m_cloudUploader.sendToCloud(windowContent);
                reset();
            }
        }

        int recordsWithoutWm = watermarkFound ? (recordsDrainedSize-m_waterMarkEntry.getPayloadInBytes()) : recordsDrainedSize;
        m_recentEpochRecordsDrainedSize.set(recordsWithoutWm);
    }

    // TODO: evaluate synchronous vs. asynchronous design, don't use locks here if possible
    public void put(IData item) {
        try {
            if(lastPutWasWatermark.get()) {
                // TODO: If take() isn't called on happy path right after drain finishes, it can be
                // TODO: called again and again for each new record, which is a No-op. Can optimize
                tryDrainTillWatermark();
                JarvisLogger.debug(m_queueId + " [put] Queue size on put " + size() + ", records drianed size: "
                        + m_recentEpochRecordsDrainedSize.get());
                m_recentIdleTimeInMs.set(System.currentTimeMillis() - m_startIdleTimer.get());
                m_startIdleTimer.set(Long.MAX_VALUE);
                lastPutWasWatermark.set(false);
                if(m_selfObserve.get()) {
                    m_sampleWmSeqNum.set(true);
                }
            }

            double randNum = randGen.nextDouble();
            synchronized (probSendingToEdgeLock) {
                if (!m_disableSendingToEdge.get() && randNum < m_probSendingToEdge) {
                    queue.put(item);
                } else {
                    // Discard the record to cloud
                    item.writeSelfToKryo(m_kryo, m_networkOutput);
                    m_currEpochRecordsDiscardedSize.addAndGet(item.getPayloadInBytes());
                }
            }

        } catch (Exception ex) {
            JarvisLogger.debug(m_queueId + " [MY DEBUG] Exception in queueuing " + ex.toString());
            ex.printStackTrace();
        }
    }

    public void putWaterMark(IData watermark) {
        try {
            synchronized (watermarkLock) {
                while(waterMarkCountInQueue.get() > 0) {
                    watermarkLock.wait();
                }

                waterMarkCountInQueue.incrementAndGet();
                queue.put(watermark);
                drainageTriggered.set(false);
                m_recentEpochTimeInMs.set(System.currentTimeMillis() - m_startEpochTimer.get());
                m_startEpochTimer.set(System.currentTimeMillis());
                m_prevEpochRecordsDiscardedSize.set(m_currEpochRecordsDiscardedSize.getAndSet(0));
                JarvisLogger.debug(m_queueId + " [putWaterMark] Watermark count in putWaterMark " +
                        waterMarkCountInQueue.get() + " and " + " size is " + size() + ", watermark seq num: " + watermark.getSeqNum());
            }

            if(m_sampleWmSeqNum.get()) {
                m_selfObserve.set(false);
                m_sampleWmSeqNum.set(false);

                synchronized (m_newEpochLock) {
                    m_recentSeqNum.set(watermark.getSeqNum());
                    m_newEpochLock.notify();
                }
            }


//            m_networkQueue.put(watermark);
//            PingMeshKryo waterMarkEntryKryoOnly = ((PingMeshKryoWithTime)watermark).getEntity();
//            m_kryo.writeObject(m_networkOutput, waterMarkEntryKryoOnly);
//            m_cloudUploader.sendToCloud();
            lastPutWasWatermark.set(true);
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in watermarking " + ex.toString());
            ex.printStackTrace();
        }
    }

    public int waitForNewEpochAndGetSeqNum() {
        int seqNum = -1;
        m_selfObserve.set(true);
        try {
            synchronized (m_newEpochLock) {
                while (m_recentSeqNum.get() == -1) {
                    m_newEpochLock.wait();
                }
            }

            seqNum = m_recentSeqNum.getAndSet(-1);
            JarvisLogger.debug("[ControllerQueue.waitForNewEpochAndGetSeqNum] Seq num is : " + seqNum);
        } catch (Exception ex) {
            JarvisLogger.debug("[ControllerQueue.waitForNewEpochAndGetSeqNum] Failed to get seq num: " + ex.toString());
            ex.printStackTrace();
        }

        return seqNum;
    }

    public int size() {
        return queue.size();
    }

    public byte[] getByteArrayOutputStreamContent() {
        try {
            m_networkOutput.flush();
            m_networkOutputStream.flush();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
            ex.printStackTrace();
        }

        return m_networkOutputStream.toByteArray();
    }

    public void reset() {
        m_networkOutputStream.reset();
        m_networkOutput.reset();
    }

    public void clear() {
        queue.clear();
        try {
            m_networkOutputStream.flush();
            m_networkOutput.flush();
            m_networkOutputStream.reset();
            m_networkOutput.reset();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
            ex.printStackTrace();
        }
    }

    // Runtime related functions
    public void disableSendingToEdge() {
        m_disableSendingToEdge.set(true);
        setProbSendingToEdge(0);
    }

    public void enableSendingToEdgeFlagOnly() {
        m_disableSendingToEdge.set(false);
    }

    public void enableSendingToEdge() {
        enableSendingToEdgeFlagOnly();
        setProbSendingToEdge(1);
    }

//    public void blockDraining() {
//        m_drainBlocked.set(true);
//    }

//    public void unblockDraining() {
//        m_drainBlocked.set(false);
//    }

    public void setSelfObserve() {
        m_selfObserve.set(true);
    }

    public void setProbSendingToEdge(double probSendingToEdge) {
        synchronized (probSendingToEdgeLock) {
            m_probSendingToEdge = probSendingToEdge;
        }
    }

    public double getProbSendingToEdge() {
        double probReturnVal;
        synchronized (probSendingToEdgeLock) {
            probReturnVal = m_probSendingToEdge;
        }

        return probReturnVal;
    }

    public int getRecentRecordsDrainedSize() {
        int drainedRec = m_recentEpochRecordsDrainedSize.get();
        m_recentEpochRecordsDrainedSize.set(0);
        return drainedRec;
    }

    public int getRecentRecordsDiscardedSize() {
        return m_prevEpochRecordsDiscardedSize.get();
    }

    public long getRecentIdleTime() {
        long idleTime = m_recentIdleTimeInMs.get();
        m_recentIdleTimeInMs.set(0);
        return idleTime;

    }

    public long getRecentEpochTime() {
        return m_recentEpochTimeInMs.get();
    }

    public double getRecentPayloadSizeOnEdge() {
        double payloadSize = m_prevEpochDataSizeSentToEdge;
        m_prevEpochDataSizeSentToEdge = 0;
        return payloadSize;
    }

    public void waitForWatermarkWithSeqNum(int seqNum) {
        // No op
        throw new UnsupportedOperationException();
    }

    public void unseeWatermark() {
        // No op
        throw new UnsupportedOperationException();
    }
}
