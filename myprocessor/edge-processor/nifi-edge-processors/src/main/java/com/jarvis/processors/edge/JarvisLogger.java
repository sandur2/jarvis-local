package com.jarvis.processors.edge;
import org.apache.nifi.logging.ComponentLog;

import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class JarvisLogger {
    static ComponentLog m_logger;
    public static void initialize(ComponentLog logger) {
        m_logger = logger;
    }

    public static void debug(String s) {
        SimpleDateFormat formatOnly = new SimpleDateFormat("HH:mm:ss");
        String time = formatOnly.format(Calendar.getInstance().getTime());
        m_logger.debug("[ATUL DEBUG]" + time + ": " + s);
    }

    public static void info(String s) {
        m_logger.info("[ATUL INFO] " + s);
    }
}
