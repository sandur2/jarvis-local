package com.jarvis.processors.edge;

public enum RuntimeAction {
    DATA_INC,
    DATA_DEC,
    OP_INC,
    OP_DEC,
    NO_ACTION,
}
