package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;

import java.io.ByteArrayOutputStream;

public class PingMeshKryo implements IData {
    int m_constantCol;
    int m_event_time;
    int m_src_cluster;
    int m_src_ip;
    int m_src_port;
    int m_dest_cluster;
    int m_dest_ip;
    int m_dest_port;
    int m_seq_num;
    int m_rtt;
    int m_errcode;
    int m_result_type;
    int m_level;
    int m_msglen;
//    public static volatile int instanceSize = 0;

    public int getSeqNum() {
        return m_seq_num;
    }

    public void setSeqNum(int seqNum) {
        m_seq_num = seqNum;
    }

    public String toString() {
        return m_constantCol + "," + m_event_time + "," + m_src_cluster + "," + m_src_ip +
                "," + m_src_port + "," + m_dest_cluster + "," + m_dest_ip + "," +
                m_dest_port + "," + m_seq_num + "," + m_rtt + "," + m_errcode + "," +
                m_result_type + "," + m_level + "," + m_msglen +  "\n";
    }

    public int getPayloadInBytes() {
        return 14 * Runtime.SIZE_OF_INT;
//        if(instanceSize == 0) {
//            instanceSize = calculateInstanceSizeWithKryo();
//        }
//
//        return instanceSize;
    }

    public boolean isWaterMark() {
        return (this.m_constantCol == Integer.MIN_VALUE);
    }


    public long getQueueTime() { return -1; }
    public void resetQueueTime() {}
    public IData getEntity() { return null; }
    public void setEntity(IData data) {}

    public void writeSelfToKryo(Kryo kryo, Output output) {
        // No-op
    }

//    private int calculateInstanceSizeWithKryo() {
//        Kryo kryo = new Kryo();
//        kryo.register(PingMeshKryo.class);
//        PingMeshKryo pingMeshKryo = new PingMeshKryo();
//        pingMeshKryo.m_src_cluster = 23;
//        pingMeshKryo.m_errcode = 232;
//        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
//        Output output = new Output(outputStream);
//        kryo.writeObject(output, pingMeshKryo);
//        output.flush();
//        try {
//            outputStream.flush();
//        } catch (Exception ex) {
//            System.out.println("There is an exception flushing out in getting payload size of PingMeshKryo");
//        }
//
//        return outputStream.toByteArray().length;
//    }
}
