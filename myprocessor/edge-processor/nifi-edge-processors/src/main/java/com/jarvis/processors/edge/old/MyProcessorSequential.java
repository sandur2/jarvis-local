///*
// * Licensed to the Apache Software Foundation (ASF) under one or more
// * contributor license agreements.  See the NOTICE file distributed with
// * this work for additional information regarding copyright ownership.
// * The ASF licenses this file to You under the Apache License, Version 2.0
// * (the "License"); you may not use this file except in compliance with
// * the License.  You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package com.jarvis.processors.edge;
//
//import com.esotericsoftware.kryo.Kryo;
//import com.esotericsoftware.kryo.io.Input;
//import org.apache.nifi.annotation.behavior.ReadsAttribute;
//import org.apache.nifi.annotation.behavior.ReadsAttributes;
//import org.apache.nifi.annotation.behavior.WritesAttribute;
//import org.apache.nifi.annotation.behavior.WritesAttributes;
//import org.apache.nifi.annotation.documentation.CapabilityDescription;
//import org.apache.nifi.annotation.documentation.SeeAlso;
//import org.apache.nifi.annotation.documentation.Tags;
//import org.apache.nifi.annotation.lifecycle.OnScheduled;
//import org.apache.nifi.components.PropertyDescriptor;
//import org.apache.nifi.flowfile.FlowFile;
//import org.apache.nifi.logging.LogLevel;
//import org.apache.nifi.processor.*;
//import org.apache.nifi.processor.exception.ProcessException;
//import org.apache.nifi.processor.io.InputStreamCallback;
//import org.apache.nifi.processor.io.OutputStreamCallback;
//import org.apache.nifi.processor.util.StandardValidators;
//import org.apache.nifi.logging.ComponentLog;
//import org.apache.nifi.util.NiFiProperties;
//
//import java.io.*;
//import java.util.*;
//import java.util.concurrent.atomic.AtomicReference;
//
//@Tags({"example"})
//@CapabilityDescription("Provide a description")
//@SeeAlso({})
//@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
//@WritesAttributes({@WritesAttribute(attribute="", description="")})
//public class MyProcessor extends AbstractProcessor {
//
//    public static final PropertyDescriptor MY_PROPERTY = new PropertyDescriptor
//            .Builder().name("MY_PROPERTY")
//            .displayName("My property")
//            .description("Example Property")
//            .required(true)
//            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
//            .build();
//
//    public static final Relationship MY_RELATIONSHIP = new Relationship.Builder()
//            .name("MY_RELATIONSHIP")
//            .description("Example relationship")
//            .build();
//
//    public static final Relationship MY_RELATIONSHIP_1 = new Relationship.Builder()
//            .name("MY_RELATIONSHIP_1")
//            .description("Example relationship 1")
//            .build();
//
//    private List<PropertyDescriptor> descriptors;
//
//    private Set<Relationship> relationships;
//
//    // Custom Jarvis code
//    private ComponentLog m_logger;
////    CustomOperator mapOp;
//    QueueWrapper<PingMeshKryoWithTime> m_operatorQueue;
//    OutputQueueWrapper<PingMeshKryo> m_exitQueue;
//    PingMeshKryoWithTimePool m_pool;
//    NiFiProperties m_nifiProperties;
//    Kryo m_kryo;
//    int m_numOperators;
//
//    @Override
//    protected void init(final ProcessorInitializationContext context) {
//        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
//        descriptors.add(MY_PROPERTY);
//        this.descriptors = Collections.unmodifiableList(descriptors);
//
//        final Set<Relationship> relationships = new HashSet<Relationship>();
//        relationships.add(MY_RELATIONSHIP);
//        relationships.add(MY_RELATIONSHIP_1);
//        this.relationships = Collections.unmodifiableSet(relationships);
//
//        // Custom Jarvis code
//        this.m_logger=getLogger();
//        m_kryo = new Kryo();
//        m_kryo.register(PingMeshKryo.class);
//        m_kryo.register(PingMeshKryoWithTime.class);
//        m_kryo.register(Integer.class);
//        m_kryo.register(SrcClusterStatsKryo.class);
//        String queueThresholdConfig = "300";
//        m_logger.log(LogLevel.INFO, "[Atul] Queue threshold config is " + queueThresholdConfig);
//        m_operatorQueue = new QueueWrapper<>(Integer.parseInt(queueThresholdConfig), m_logger, m_kryo);
////        m_operatorQueue.initializeKryo(new PingMeshKryoWithTime(), new PingMeshKryo());
//        m_exitQueue = new OutputQueueWrapper<>(m_logger, m_kryo);
////        m_exitQueue.initializeKryo(new PingMeshKryo());
//
//        m_pool = new PingMeshKryoWithTimePool(148545);
////        m_nifiProperties = new NiFiProperties() {
////            @Override
////            public String getProperty(String s) {
////                return NiFiProperties.VARIABLE_REGISTRY_PROPERTIES;
////            }
////
////            @Override
////            public Set<String> getPropertyKeys() {
////                return null;
////            }
////        };
//
////        String queueThresholdConfig = m_nifiProperties.getProperty(null);
////        String queueThresholdConfig = "150000";
//        m_numOperators = 1;
////        m_operatorQueue = new QueueWrapper<>(Integer.parseInt(queueThresholdConfig));
////        m_operatorQueue.initializeKryo(new PingMeshKryoWithTime(), new PingMeshKryo());
////        m_exitQueue = new OutputQueueWrapper<>();
////        m_exitQueue.initializeKryo(new PingMeshKryo());
//    }
//
//    @Override
//    public Set<Relationship> getRelationships() {
//        return this.relationships;
//    }
//
//    @Override
//    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
//        return descriptors;
//    }
//
//    @OnScheduled
//    public void onScheduled(final ProcessContext context) {
//
//    }
//
//    @Override
//    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
//        FlowFile flowFile = session.get();
//        // To get multiple flowfiles
//        // session.get(context.getProperty("BATCH_SIZE").asInteger());
//        if ( flowFile == null ) {
//            return;
//        }
//
//        // TODO implement
//
//        // Write current timestamp to get queueing time
//        long queueEndTime = System.currentTimeMillis();
//
//        // Profiling operators
//        long[] startExtract = {0};
//        long[] endExtract = {0};
//
//        Input[] input = new Input[1];
//        Thread[] mapThread = new Thread[1];
//
//        CustomMapOperator mapOp = new CustomMapOperator(m_operatorQueue, this.m_logger);
//        mapOp.setExitQueue(m_exitQueue);
//
//        session.read(flowFile, new InputStreamCallback() {
//            @Override
//            public void process(InputStream inputStream) throws IOException {
//                try{
//                    System.setProperty("rx.ring-buffer.size", "150000");
//                    mapThread[0] = new Thread(mapOp);
//                    mapThread[0].start();
//
//                    PingMeshKryo waterMark = new PingMeshKryo();
//                    waterMark.m_constantCol= Integer.MIN_VALUE;
//                    PingMeshKryoWithTime waterMarkWithTime = new PingMeshKryoWithTime();
//                    waterMarkWithTime.setEntity(waterMark);
//
//                    // Reading the data
//                    input[0] = new Input(inputStream);
//                    startExtract[0] = System.currentTimeMillis();
//                    Integer numRecords = (Integer) m_kryo.readObject(input[0], Integer.class);
//                    PingMeshKryo object2 = m_kryo.readObject(input[0], PingMeshKryo.class);
//                    m_pool.pingMeshKryoWithTimes[0].setEntity(object2);
//                    m_operatorQueue.put(m_pool.pingMeshKryoWithTimes[0]);
//                    int read = 1;
//                    while(read < numRecords) {
//                        object2 = m_kryo.readObject(input[0], PingMeshKryo.class);
//                        m_pool.pingMeshKryoWithTimes[read].setEntity(object2);
//                        m_operatorQueue.put(m_pool.pingMeshKryoWithTimes[read]);
//                        read++;
//                    }
//
//                    endExtract[0] = System.currentTimeMillis();
//                    m_operatorQueue.putWaterMark(waterMarkWithTime);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    System.out.println("File not found exception");
//                }
//
//            }
//        });
//
//        // Close the input stream before joining on the thread
//        input[0].close();
//
//        try {
//            mapThread[0].join();
////            mapOp.run();
//        } catch (Exception ex){
//            this.m_logger.debug("[MY DEBUG] Joining on grp and agg threads failed");
//        }
//
////        m_exitQueue.m_output.close();
////        m_operatorQueue.m_networkOutput.close();
//
//        this.m_logger.debug("[MY DEBUG] The sizes of exit/network queue are " + m_exitQueue.m_outputStream.size() + " and " +
//                m_operatorQueue.m_networkOutputStream.size());
//
//        long end = System.currentTimeMillis();
//
//        // Write extraction time
//        flowFile = session.putAttribute(flowFile, "m_extractDuration", String.valueOf(endExtract[0]-startExtract[0]));
//
//        // Write groupby time to flowfile attribute
//        flowFile = session.putAttribute(flowFile, "m_queueEndTime", String.valueOf(queueEndTime));
//
//        // Write total query time to flowfile attribute
//        flowFile = session.putAttribute(flowFile, "m_totalDuration", String.valueOf(end-queueEndTime));
//
//        flowFile = session.putAttribute(flowFile, "m_recordCount", String.valueOf(mapOp.getOutputRecCount()[0]));
//        // Write record count to flowfile attribute
//
//        // Write operator data
//        flowFile = session.putAttribute(flowFile, "m_grpbyop_maxQTime", String.valueOf(mapOp.m_maxQueueTime));
//        flowFile = session.putAttribute(flowFile, "m_grpbyop_avgProcTime", String.valueOf(mapOp.m_avgProcessingTime));
//        flowFile = session.putAttribute(flowFile, "m_grpbyop_dfbuilddur", String.valueOf(mapOp.m_dataflowBuildDur));
//
//        // To write the results back out ot flow file
//        flowFile = session.write(flowFile, new OutputStreamCallback() {
//            @Override
//            public void process(OutputStream out) throws IOException {
//                out.write(m_exitQueue.getByteArrayOutputStream().toByteArray());
//            }
//        });
//
//        session.transfer(flowFile,MY_RELATIONSHIP);
//
//        // Write out to network flowfiles
//        byte[] operatorNetworkQueueContent = m_operatorQueue.getNetworkByteArrayOutputStream().toByteArray();
//        if(operatorNetworkQueueContent.length > 0) {
//            final AtomicReference<FlowFile> opNetworkFlowFile = new AtomicReference<>(session.create());
//            opNetworkFlowFile.set(session.putAttribute(opNetworkFlowFile.get(),
//                    "m_network_type", "op1NetworkFlowfile"));
//            opNetworkFlowFile.set(session.write(opNetworkFlowFile.get(), new OutputStreamCallback() {
//                @Override
//                public void process(OutputStream outNetwork) throws IOException {
//                    outNetwork.write(operatorNetworkQueueContent);
//                }
//            }));
//
//            session.transfer(opNetworkFlowFile.get(), MY_RELATIONSHIP_1);
//        }
//
//        m_operatorQueue.clear();
//        m_exitQueue.clear();
//    }
//}
