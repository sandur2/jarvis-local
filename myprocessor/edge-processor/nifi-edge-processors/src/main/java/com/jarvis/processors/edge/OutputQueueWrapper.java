package com.jarvis.processors.edge;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.apache.nifi.processor.Relationship;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.atomic.AtomicInteger;

public class OutputQueueWrapper implements IQueue {
    ByteArrayOutputStream m_outputStream;
    Output m_output;
    Kryo m_kryo;
    AtomicInteger m_size;
    CloudUploader m_cloudUploader;

    public volatile double m_prevWindowDataSizeSentToEdge = 0;
    double m_currentWindowDataSizeSentToEdge = 0;
    private boolean m_watermarkObserved;
    private Object m_waterMarkInExitLock;
    private Integer m_recentWaterMarkSeqNum = 0;

    public OutputQueueWrapper(Kryo kryo, Relationship MY_RELATIONSHIP) {
//        m_kryo = new Kryo();
        m_kryo = kryo;
        m_outputStream = new ByteArrayOutputStream();
        m_output = new Output(m_outputStream);
        m_cloudUploader = new CloudUploader(MY_RELATIONSHIP);
        m_waterMarkInExitLock = new Object();
        m_watermarkObserved = false;
    }

    public void initializeKryo(Object ...classObj) {
        for (Object classObjInst :
                classObj) {
            m_kryo.register(classObjInst.getClass());
        }
    }

    public double getRecentPayloadSizeOnEdge() {
        return m_prevWindowDataSizeSentToEdge;
    }

    public void put(IData item) {
        try {
            item.writeSelfToKryo(m_kryo, m_output);
            m_currentWindowDataSizeSentToEdge += item.getPayloadInBytes();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in queueuing " + ex.toString());
        }
    }

    public void putWaterMark(IData item) {
        try {
            item.writeSelfToKryo(m_kryo, m_output);
            byte[] windowContent = getByteArrayOutputStreamContent();
            this.reset();
            new Thread(() -> m_cloudUploader.sendToCloud(windowContent)).start();
            m_prevWindowDataSizeSentToEdge = m_currentWindowDataSizeSentToEdge;
            m_currentWindowDataSizeSentToEdge = 0;
            JarvisLogger.debug("[ExitQueue.putWaterMark] Size of recent window in output queue is " + m_prevWindowDataSizeSentToEdge);
            synchronized (m_waterMarkInExitLock) {
                m_watermarkObserved = true;
                m_recentWaterMarkSeqNum = item.getSeqNum();
                m_waterMarkInExitLock.notify();
            }
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in queueuing " + ex.toString());
        }
    }

    public int getRecentSeqNum() {
        int seqNum;
        synchronized (m_waterMarkInExitLock) {
            seqNum = m_recentWaterMarkSeqNum;
        }

        return seqNum;
    }

    public byte[] getByteArrayOutputStreamContent() {
        try {
            m_output.flush();
            m_outputStream.flush();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing output queue wrapper: " + ex.toString());
        }

        return m_outputStream.toByteArray();
    }

    public void reset() {
        m_outputStream.reset();
        m_output.reset();
    }

    public void clear() {
        try {
            m_outputStream.flush();
            m_output.flush();
            m_outputStream.reset();
            m_output.reset();
            m_size.set(0);
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
        }
    }

    public void unseeWatermark() {
        synchronized (m_waterMarkInExitLock) {
            m_watermarkObserved = false;
        }
    }

    public void waitForWatermarkWithSeqNum(int seqNum) {
        try {
            synchronized (m_waterMarkInExitLock) {
                while (!(m_watermarkObserved &&
                        ((seqNum==-1) || (m_recentWaterMarkSeqNum >= seqNum)))) {
                    m_waterMarkInExitLock.wait();
                }
            }

            JarvisLogger.debug("[OutputQueueWrapper.waitForWatermarkWithSeqNum] Seq num is: " + seqNum);
        } catch (Exception ex) {
            JarvisLogger.debug("Unhandled exception when waiting for exit watermark: " + ex.toString());
        }
    }

    public int waitForNewEpochAndGetSeqNum() {
        // No op
        throw new UnsupportedOperationException();
    }

    public long getRecentIdleTime() {
        // No op
        throw new UnsupportedOperationException();
    }

    public int getRecentRecordsDrainedSize() {
        // No op
        throw new UnsupportedOperationException();
    }

    public long getRecentEpochTime() {
        // No op
        throw new UnsupportedOperationException();
    }

    public void enableSendingToEdge() {
        // No op
//        throw new UnsupportedOperationException();
    }

    public void setProbSendingToEdge(double probSendingToEdge) {
        // No op
        throw new UnsupportedOperationException();
    }

    public void enableSendingToEdgeFlagOnly() {
        // No op
        throw new UnsupportedOperationException();
    }

    public void disableSendingToEdge() {
        // No op
        throw new UnsupportedOperationException();
    }

    public int getRecentRecordsDiscardedSize() {
        // No op
        throw new UnsupportedOperationException();
    }
}
