package com.jarvis.processors.edge.old;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import com.jarvis.processors.edge.CloudUploader;
import com.jarvis.processors.edge.IData;
import com.jarvis.processors.edge.JarvisLogger;
import org.apache.nifi.processor.Relationship;

import java.io.ByteArrayOutputStream;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class SyncQueueWrapper {
    int m_queueId;
    LinkedBlockingQueue<IData> queue = new LinkedBlockingQueue<>();
//    LinkedBlockingQueue<T> m_networkQueue = new LinkedBlockingQueue<>();
    ByteArrayOutputStream m_networkOutputStream;
    Output m_networkOutput;
    Kryo m_kryo;
    int m_threshold;
    CloudUploader m_cloudUploader;
    AtomicInteger waterMarkCountInQueue = new AtomicInteger(0);
    AtomicBoolean drainageTriggered = new AtomicBoolean(false);
    AtomicBoolean lastPutWasWatermark = new AtomicBoolean(false);
    AtomicBoolean m_drainageJustFinished = new AtomicBoolean(false);
    AtomicInteger watermarksDrained = new AtomicInteger(0);
    Object watermarkLock = new Object();
    AtomicInteger m_recentRecordsDrainedSize = new AtomicInteger(0);
    AtomicLong m_startIdleTimer = new AtomicLong(Long.MAX_VALUE);
    AtomicLong m_recentIdleTimeInMs = new AtomicLong(0);

    // Control proxy variables
    volatile double m_probSendingToEdge = 1;
    AtomicBoolean m_disableSendingToEdge = new AtomicBoolean(false);
    public volatile double m_prevWindowDataSizeSentToEdge = 0;
    private double m_currentWindowDataSizeSentToEdge = 0;
    private IData m_prevRecordDequeuedAndCached = null;
//    private AtomicBoolean m_drainBlocked = new AtomicBoolean(true);

    // Required when you want to drain out the queue after new flowfile arrives
    // and there are pending items in queue
    Object queueReadLock = new Object();
    Object probSendingToEdgeLock = new Object();

    Random randGen = new Random();

    IData m_waterMarkEntry;

    public SyncQueueWrapper(int queueId, int threshold, Kryo kryo, Relationship MY_RELATIONSHIP, IData watermarkEntry) {
        m_queueId = queueId;
        m_threshold = threshold;
//        m_kryo = new Kryo();
        m_kryo = kryo;
        m_networkOutputStream = new ByteArrayOutputStream();
        m_networkOutput = new Output(m_networkOutputStream);
        m_cloudUploader = new CloudUploader(MY_RELATIONSHIP);
        m_waterMarkEntry = watermarkEntry;
        new Thread(() -> tryDrainTillWatermark()).start();
    }

    public void initializeKryo(Object ...classObj) {
        for (Object classObjInst :
                classObj) {
            m_kryo.register(classObjInst.getClass());
        }
    }

    public boolean isEntryWatermark(IData entry) {
        boolean isWaterMark = false;
        if(entry != null) {
            isWaterMark = entry.isWaterMark();
        }

        return isWaterMark;
    }

//    public T take() {
//        T takenVal = null;
//        try {
//            if(waterMarkInQueue.get()) {
//                synchronized (watermarkLock) {
//                    while(waterMarkInQueue.get() && ((takenVal=queue.poll()) == null)) {
//                        watermarkLock.wait();
//                    }
//                    if (isEntryWatermark(takenVal)) {
//                        waterMarkInQueue.set(false);
//                    }
//                }
//            }
//
//            if(takenVal == null) {
//                takenVal = queue.take();
//            }
//
//        } catch (Exception ex) {
//            JarvisLogger.debug("[MY DEBUG] Exception in dequeueuing " + ex.toString());
//        }
//
//        return takenVal;
//    }

    public IData take() {
        IData takenVal = null;
        boolean watermarkCreated = false;
        try{
            if(m_prevRecordDequeuedAndCached != null) {
                takenVal = m_prevRecordDequeuedAndCached;
                m_prevRecordDequeuedAndCached = null;
            }

            if(takenVal == null) {
                takenVal = queue.take();
                synchronized (watermarkLock) {
                    if (watermarksDrained.get() > 0) {
                        m_prevRecordDequeuedAndCached = takenVal;
                        if (takenVal.isWaterMark()) {
                            JarvisLogger.debug("[SyncQueue.take] [ERROR] A watermark was cached, assertion error");
                        }

                        // For watermarks that are already drained, close epoch in dataflow
                        takenVal = m_waterMarkEntry;
                        watermarksDrained.decrementAndGet();
//                        waterMarkCountInQueue.decrementAndGet();
                        watermarkCreated = true;
                    }

                    if (takenVal.isWaterMark()) {
                        waterMarkCountInQueue.decrementAndGet();
                        drainageTriggered.set(true);
                        JarvisLogger.debug(m_queueId + " [SyncQueue.take] After reading outside of lock, " +
                                "watermark size in take is " + waterMarkCountInQueue.get());
                    }
//                    else if (waterMarkCountInQueue.get() > 0) {
//                        takenVal = queue.take();
//                        if (isEntryWatermark(takenVal)) {
//                            waterMarkCountInQueue.decrementAndGet();
//                            drainageTriggered.set(true);
//                            JarvisLogger.debug(m_queueId + " [SyncQueue.take] Watermark size in take is "+
//                                    waterMarkCountInQueue.get());
//                        }
//                    }
                }
            }

//            if(takenVal == null) {
//                takenVal = queue.take();
//                //            if(waterMarkCountInQueue.get() > 0 || watermarksDrained.get() > 0) {
//                if (isEntryWatermark(takenVal)) {
//                    synchronized (watermarkLock) {
//                        waterMarkCountInQueue.decrementAndGet();
//                        drainageTriggered.set(true);
//                        JarvisLogger.debug(m_queueId + " [SyncQueue.take] After reading outside of lock, " +
//                                "watermark size in take is " + waterMarkCountInQueue.get());
//                    }
//                }
//            }
        } catch (Exception ex) {
            JarvisLogger.debug("[SyncQueue.take] Exception in dequeueuing " + ex.toString());
        }

        if(isEntryWatermark(takenVal)) {
            // Reset the window size to signify end of current window processing by operator
            m_prevWindowDataSizeSentToEdge = m_currentWindowDataSizeSentToEdge;
            m_currentWindowDataSizeSentToEdge = 0;
            if(!watermarkCreated) {
                m_startIdleTimer.compareAndSet(Long.MAX_VALUE, System.currentTimeMillis());
            }
            JarvisLogger.debug(m_queueId + " [SyncQueue.take] Size of recent window processed is " +
                    m_prevWindowDataSizeSentToEdge);
        } else {
            m_currentWindowDataSizeSentToEdge += takenVal.getPayloadInBytes();
        }

        return takenVal;
    }

    // TODO: Run with thread pool instead of creating new thread
    // TODO: Evaluate with async drainage too
    private void tryDrainTillWatermark() {
        long start = System.currentTimeMillis();
        IData takenVal;
        int numRecords = 0;
        int recordsDrainedSize = 0;

        // Additional condition: && !m_drainBlocked.get()
        if(waterMarkCountInQueue.get() > 0 && !drainageTriggered.get()) {
            synchronized (watermarkLock) {
                // Additional condition: && !m_drainBlocked.get()
                if(waterMarkCountInQueue.get() > 0 && !drainageTriggered.get()) {
                    JarvisLogger.debug(m_queueId + "[tryDrainTillWatermark] Watermark count for take is " +
                            waterMarkCountInQueue.get() + ", Queue size is " + size());
                    boolean watermarkFound = false;
                    while (!watermarkFound && (takenVal = queue.poll()) != null) {
                        // Transfer all queue contents for current window to cloud upload
                        if (isEntryWatermark(takenVal)) {
                            watermarkFound = true;
                            watermarksDrained.incrementAndGet();
//                            try {
//                                queue.put(takenVal);
//                            } catch (Exception ex) {
//                                JarvisLogger.debug("[SyncQueueWrapper.tryDrainTillWatermark] Couldn't re-insert watermark");
//                            }
//                            m_startIdleTimer.compareAndSet(Long.MAX_VALUE, System.currentTimeMillis());
                        }
//                        else {
//                            queue.poll();
//                        }

                        takenVal.writeSelfToKryo(m_kryo, m_networkOutput);
                        numRecords++;
                        recordsDrainedSize += takenVal.getPayloadInBytes();
                    }

                    JarvisLogger.debug(m_queueId + " [tryDrainTillWatermark] Total records ejected in this drain session is " +
                            numRecords + " and " + "watermark count for take is " + waterMarkCountInQueue.get() +
                            " and queue size is " + size());
                }

                drainageTriggered.set(true);
            }

            // If there was more than just the watermark
            if(numRecords > 1) {
                byte[] windowContent = getByteArrayOutputStreamContent();
                m_cloudUploader.sendToCloud(windowContent);
                reset();
            }

            m_recentRecordsDrainedSize.set(recordsDrainedSize);
        }

//        JarvisLogger.debug("Drain time is " + (System.currentTimeMillis() - start));
    }

    // put and putWaterMark are on the same thread. We allow either put or take to process until the watermark
    // and inform the other thread about it
//    public void put(T item) {
//        try {
//            if(waterMarkInQueue.get()) {
//                synchronized (watermarkLock) {
//                    if(waterMarkInQueue.get() && !drainageTriggered.get()) {
//                        new Thread(() -> drainTillWatermark()).start();
//                        drainageTriggered.set(true);
//                    }
//                    queue.put(item);
//                    watermarkLock.notify();
//                }
//            } else {
//                queue.put(item);
//            }
//        } catch (Exception ex) {
//            JarvisLogger.debug("[MY DEBUG] Exception in queueuing " + ex.toString());
//        }
//    }


    // TODO: evaluate synchronous vs. asynchronous design, don't use locks here if possible
    public void put(IData item) {
        try {
            // TODO: If take() isn't called on happy path right after drain finishes, it can be
            // TODO: called again and again for each new record, which is a No-op. Can optimize
            tryDrainTillWatermark();

            if(lastPutWasWatermark.get()) {
                JarvisLogger.debug(m_queueId + " [put] Queue size on put " + size());
                m_recentIdleTimeInMs.set(System.currentTimeMillis()-m_startIdleTimer.get());
                m_startIdleTimer.set(Long.MAX_VALUE);
                lastPutWasWatermark.set(false);
            }

            double randNum = randGen.nextDouble();
            synchronized (probSendingToEdgeLock) {
                if (randNum < m_probSendingToEdge && !m_disableSendingToEdge.get()) {
                    queue.put(item);
                } else {
                    // Drain the record to cloud
                    item.writeSelfToKryo(m_kryo, m_networkOutput);

                }
            }

        } catch (Exception ex) {
            JarvisLogger.debug(m_queueId + " [MY DEBUG] Exception in queueuing " + ex.toString());
        }
    }

    public void putWaterMark(IData watermark) {
        try {
            synchronized (watermarkLock) {
                waterMarkCountInQueue.incrementAndGet();
                queue.put(watermark);
                drainageTriggered.set(false);
                JarvisLogger.debug(m_queueId + " [putWaterMark] Watermark count in putWaterMark " +
                        waterMarkCountInQueue.get() + " and " + " size is " + size());
            }

//            m_networkQueue.put(watermark);
//            PingMeshKryo waterMarkEntryKryoOnly = ((PingMeshKryoWithTime)watermark).getEntity();
//            m_kryo.writeObject(m_networkOutput, waterMarkEntryKryoOnly);
//            m_cloudUploader.sendToCloud();
            lastPutWasWatermark.set(true);
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in watermarking " + ex.toString());
        }
    }

    public int size() {
        return queue.size();
    }

    public byte[] getByteArrayOutputStreamContent() {
        try {
            m_networkOutput.flush();
            m_networkOutputStream.flush();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
        }

        return m_networkOutputStream.toByteArray();
    }

    public void reset() {
        m_networkOutputStream.reset();
        m_networkOutput.reset();
    }

    public void clear() {
        queue.clear();
        try {
            m_networkOutputStream.flush();
            m_networkOutput.flush();
            m_networkOutputStream.reset();
            m_networkOutput.reset();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
        }
    }

    // Runtime related functions
    public void setDisableSendingToEdge() {
        m_disableSendingToEdge.set(true);
    }

    public void resetDisableSendingToEdge() {
        m_disableSendingToEdge.set(false);
    }

//    public void blockDraining() {
//        m_drainBlocked.set(true);
//    }

//    public void unblockDraining() {
//        m_drainBlocked.set(false);
//    }

    public void setProbSendingToEdge(double probSendingToEdge) {
        synchronized (probSendingToEdgeLock) {
            m_probSendingToEdge = probSendingToEdge;
        }
    }

    public double getProbSendingToEdge() {
        double probReturnVal;
        synchronized (probSendingToEdgeLock) {
            probReturnVal = m_probSendingToEdge;
        }

        return probReturnVal;
    }

    public int getRecentRecordsDrainedSize() {
        int recordsDrained = m_recentRecordsDrainedSize.get();
        m_recentRecordsDrainedSize.set(0);
        return recordsDrained;
    }

    public long getRecentIdleTime() {
        return m_recentIdleTimeInMs.get();
    }

    public double getRecentPayloadSizeOnEdge() {
        double payloadSize = m_prevWindowDataSizeSentToEdge;
        m_prevWindowDataSizeSentToEdge = 0;
        return payloadSize;
    }
}
