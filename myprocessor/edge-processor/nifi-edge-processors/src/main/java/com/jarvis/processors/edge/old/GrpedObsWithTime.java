package com.jarvis.processors.edge.old;

import com.jarvis.processors.edge.PingMeshKryo;

public class GrpedObsWithTime {
    public io.reactivex.observables.GroupedObservable<Integer, PingMeshKryo> m_data;
    private long timeQueued;
    public boolean m_watermark;

    public GrpedObsWithTime(io.reactivex.observables.GroupedObservable<Integer, PingMeshKryo> data) {
        m_data = data;
        m_watermark = false;
        timeQueued = System.currentTimeMillis();
    }

    public void setTimeQueued() {
        timeQueued = System.currentTimeMillis();
    }

    public long getQueueTime() {
        return (System.currentTimeMillis() - timeQueued);
    }
}

