package com.jarvis.processors.edge;

public class PingMeshKryoWithTimePool {
    public PingMeshKryoWithTime[] pingMeshKryoWithTimes;
    int currentPointer = 0;

    public PingMeshKryoWithTimePool(int numElements) {
        pingMeshKryoWithTimes = new PingMeshKryoWithTime[numElements];
        for(int i = 0; i < numElements; i++) {
            pingMeshKryoWithTimes[i] = new PingMeshKryoWithTime();
        }
    }
}
