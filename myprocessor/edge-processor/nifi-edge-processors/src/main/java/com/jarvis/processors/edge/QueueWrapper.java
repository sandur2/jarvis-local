package com.jarvis.processors.edge;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import org.apache.nifi.processor.Relationship;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class QueueWrapper {
    LinkedBlockingQueue<IData> queue = new LinkedBlockingQueue<>();
//    LinkedBlockingQueue<T> m_networkQueue = new LinkedBlockingQueue<>();
    ByteArrayOutputStream m_networkOutputStream;
    Output m_networkOutput;
    Kryo m_kryo;
    int m_threshold;
    CloudUploader m_cloudUploader;
    AtomicInteger waterMarkCountInQueue = new AtomicInteger(0);
    AtomicBoolean drainageTriggered = new AtomicBoolean(false);
    AtomicBoolean lastPutWasWatermark = new AtomicBoolean(false);
    AtomicBoolean m_drainageJustFinished = new AtomicBoolean(false);
    Object watermarkLock = new Object();

    // Required when you want to drain out the queue after new flowfile arrives
    // and there are pending items in queue
    Object queueReadLock = new Object();

    IData m_waterMarkEntry;

    public QueueWrapper(int threshold, Kryo kryo, Relationship MY_RELATIONSHIP, IData watermarkEntry) {
        m_threshold = threshold;
//        m_kryo = new Kryo();
        m_kryo = kryo;
        m_networkOutputStream = new ByteArrayOutputStream();
        m_networkOutput = new Output(m_networkOutputStream);
        m_cloudUploader = new CloudUploader(MY_RELATIONSHIP);
        m_waterMarkEntry = watermarkEntry;
        new Thread(() -> drainTillWatermark()).start();
    }

    public void initializeKryo(Object ...classObj) {
        for (Object classObjInst :
                classObj) {
            m_kryo.register(classObjInst.getClass());
        }
    }

    public boolean isEntryWatermark(IData entry) {
        boolean isWaterMark = false;
        if(entry != null) {
            PingMeshKryoWithTime entryWithTime = (PingMeshKryoWithTime) entry;
            isWaterMark = entry.isWaterMark();
        }

        return isWaterMark;
    }

//    public T take() {
//        T takenVal = null;
//        try {
//            if(waterMarkInQueue.get()) {
//                synchronized (watermarkLock) {
//                    while(waterMarkInQueue.get() && ((takenVal=queue.poll()) == null)) {
//                        watermarkLock.wait();
//                    }
//                    if (isEntryWatermark(takenVal)) {
//                        waterMarkInQueue.set(false);
//                    }
//                }
//            }
//
//            if(takenVal == null) {
//                takenVal = queue.take();
//            }
//
//        } catch (Exception ex) {
//            JarvisLogger.debug("[MY DEBUG] Exception in dequeueuing " + ex.toString());
//        }
//
//        return takenVal;
//    }

    public IData take() {
        IData takenVal = null;
        try{
            if(waterMarkCountInQueue.get() > 0) {
                synchronized (watermarkLock) {
                    if (waterMarkCountInQueue.get() > 0) {
                        takenVal = queue.take();
                        if (isEntryWatermark(takenVal)) {
                            waterMarkCountInQueue.decrementAndGet();
                        }
                    }
//                    else if(m_drainageJustFinished.get()) {
//                        takenVal = m_waterMarkEntry;
//                        m_drainageJustFinished.set(false);
//                    }
                }
            }

            if(takenVal == null) {
                takenVal = queue.take();
            }
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in dequeueuing " + ex.toString());
        }

        return takenVal;
    }

    // TODO: Run with thread pool instead of creating new thread
    // TODO: need to maintain watermark cache
    private void drainTillWatermark() {
        IData takenVal;
        synchronized (watermarkLock) {
            while (true) {
                while (waterMarkCountInQueue.get()==0) {
                    try {
                        drainageTriggered.set(false);
                        watermarkLock.wait();
                    } catch (InterruptedException ex) {
                        JarvisLogger.debug("Interrupted exception in drain thread");
                    }
                }
                int numRecords = 0;
                while (waterMarkCountInQueue.get() > 0) {
                    JarvisLogger.debug("Watermark count is " + waterMarkCountInQueue.get() +
                            ", Queue size is " + size());
                    boolean watermarkFound = false;
                    while (!watermarkFound && (takenVal = queue.peek()) != null) {
                        // Transfer all queue contents for current window to cloud upload
                        if (isEntryWatermark(takenVal)) {
                            waterMarkCountInQueue.decrementAndGet();
                            watermarkFound = true;
                        } else {
                            queue.poll();
                        }

                        PingMeshKryo entityToTransfer = (PingMeshKryo) ((PingMeshKryoWithTime) takenVal).getEntity();
                        m_kryo.writeObject(m_networkOutput, entityToTransfer);
                        numRecords++;
                    }

                    byte[] windowContent = getByteArrayOutputStreamContent();
                    m_cloudUploader.sendToCloud(windowContent);
                    reset();
                }

//                    drainageTriggered.set(false);
//                    m_drainageJustFinished.set(true);
                JarvisLogger.debug("Total records ejected in this drain session is " + numRecords + " and " +
                        "watermark count is " + waterMarkCountInQueue.get() + " and queue size is " + size());
            }
        }
    }

    // put and putWaterMark are on the same thread. We allow either put or take to process until the watermark
    // and inform the other thread about it
//    public void put(T item) {
//        try {
//            if(waterMarkInQueue.get()) {
//                synchronized (watermarkLock) {
//                    if(waterMarkInQueue.get() && !drainageTriggered.get()) {
//                        new Thread(() -> drainTillWatermark()).start();
//                        drainageTriggered.set(true);
//                    }
//                    queue.put(item);
//                    watermarkLock.notify();
//                }
//            } else {
//                queue.put(item);
//            }
//        } catch (Exception ex) {
//            JarvisLogger.debug("[MY DEBUG] Exception in queueuing " + ex.toString());
//        }
//    }

    // TODO: evaluate synchronous vs. asynchronous design, don't use locks here if possible
    public void put(IData item) {
        try {
            if(waterMarkCountInQueue.get() > 0 && !drainageTriggered.get()) {
                synchronized (watermarkLock) {
                    if(waterMarkCountInQueue.get() > 0 && !drainageTriggered.get()) {
//                        new Thread(() -> drainTillWatermark()).start();
                        drainageTriggered.set(true);
                        watermarkLock.notify();
                    }
                }
            }

            if(lastPutWasWatermark.get()) {
                JarvisLogger.debug("Queue size on put " + size());
                lastPutWasWatermark.set(false);
            }

            queue.put(item);
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in queueuing " + ex.toString());
        }
    }

    public void putWaterMark(IData watermark) {
        try {
            synchronized (watermarkLock) {
                waterMarkCountInQueue.incrementAndGet();
                queue.put(watermark);
                JarvisLogger.debug("Watermark count in putWaterMark " + waterMarkCountInQueue.get());
            }

//            m_networkQueue.put(watermark);
//            PingMeshKryo waterMarkEntryKryoOnly = ((PingMeshKryoWithTime)watermark).getEntity();
//            m_kryo.writeObject(m_networkOutput, waterMarkEntryKryoOnly);
//            m_cloudUploader.sendToCloud();
            lastPutWasWatermark.set(true);
            JarvisLogger.debug("Queue size on watermark is " + size());
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG] Exception in watermarking " + ex.toString());
        }
    }

    public int size() {
        return queue.size();
    }

    public byte[] getByteArrayOutputStreamContent() {
        try {
            m_networkOutput.flush();
            m_networkOutputStream.flush();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
        }

        return m_networkOutputStream.toByteArray();
    }

    public void reset() {
        m_networkOutputStream.reset();
        m_networkOutput.reset();
    }

    public void clear() {
        queue.clear();
        try {
            m_networkOutputStream.flush();
            m_networkOutput.flush();
            m_networkOutputStream.reset();
            m_networkOutput.reset();
        } catch (Exception ex) {
            JarvisLogger.debug("[MY DEBUG]Error while clearing queue wrapper: " + ex.toString());
        }
    }

    public int waitForNewEpochAndGetSeqNum() {
        // No op
        throw new UnsupportedOperationException();
    }

    public long getRecentIdleTime() {
        // No op
        throw new UnsupportedOperationException();
    }

    public int getRecentRecordsDrainedSize() {
        // No op
        throw new UnsupportedOperationException();
    }

    public long getRecentEpochTime() {
        // No op
        throw new UnsupportedOperationException();
    }

    public void enableSendingToEdge() {
        // No op
        throw new UnsupportedOperationException();
    }

    public void setProbSendingToEdge(double probSendingToEdge) {
        // No op
        throw new UnsupportedOperationException();
    }

    public void enableSendingToEdgeFlagOnly() {
        // No op
        throw new UnsupportedOperationException();
    }

    public void disableSendingToEdge() {
        // No op
        throw new UnsupportedOperationException();
    }

    public int getRecentRecordsDiscardedSize() {
        // No op
        throw new UnsupportedOperationException();
    }
}
