package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Output;
import com.jarvis.processors.edge.PingMeshKryo;
import com.jarvis.processors.edge.PingMeshKryoWithTime;
import com.jarvis.processors.edge.SrcClusterStatsKryo;
import com.jarvis.processors.edge.old.GrpedObsWithTime;
import io.reactivex.disposables.Disposable;
import org.apache.nifi.logging.ComponentLog;

import java.util.concurrent.LinkedBlockingQueue;

public class CustomAggOperator implements Runnable {
    LinkedBlockingQueue<GrpedObsWithTime> m_currentQueue;
    LinkedBlockingQueue<SrcClusterStatsKryo> m_nextQueue;
    io.reactivex.subjects.PublishSubject<io.reactivex.observables.GroupedObservable<Integer, PingMeshKryo>> m_subject = io.reactivex.subjects.PublishSubject.create();
    long totalProcessingTime = 0;
    ComponentLog m_logger;
    long m_maxQueueTime = 0;
    float m_avgProcessingTime = 0;
    long m_dataflowBuildDur = 0;
    Output m_output;
    Kryo m_kryo;
    Integer[] m_numOutRecords = {0};

    public CustomAggOperator(LinkedBlockingQueue<GrpedObsWithTime> currentQueue, ComponentLog logger) {
        m_currentQueue = currentQueue;
        m_logger = logger;
        Long[] processStart = {0L};
        Long[] processDur = {0L};
        Long[] flapMapDur = {0L};
        Long startDataflowBuild = System.currentTimeMillis();
        m_subject.
                doOnNext(v -> {
                    processStart[0] = System.currentTimeMillis();
                }).
                flatMapSingle(grps -> {
                    return grps.toList().map(grpList -> {
//                            System.out.println("Group is " + grps.getKey());
                        int max = 0, min = Integer.MAX_VALUE, count=0;
                        float avg = 0;
                        for (PingMeshKryo data :
                                grpList) {
                            if(data.m_rtt > max) {
                                max = data.m_rtt;
                            }

                            if(data.m_rtt < min) {
                                min = data.m_rtt;
                            }

                            avg += data.m_rtt;
                            count++;
                        }

                        SrcClusterStatsKryo srcClusterStatsKryo = new SrcClusterStatsKryo();
                        srcClusterStatsKryo.m_src_cluster = grps.getKey();
                        srcClusterStatsKryo.avgRtt = (float)avg/(float)count;
                        srcClusterStatsKryo.minRtt = min;
                        srcClusterStatsKryo.maxRtt = max;
                        return srcClusterStatsKryo;
                    });
                }).
                doOnNext(v -> {
//                    processDur[0] = System.currentTimeMillis() - processStart[0];
//                    System.out.println("Right after flatmap: " + processDur[0]);
                    totalProcessingTime += (System.currentTimeMillis() - processStart[0]);
                }).subscribe(
                new io.reactivex.Observer<SrcClusterStatsKryo>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onComplete() {
//                                m_logger.debug("On complete called in subscriber with current queue size:" +
//                                        m_currentQueue.size() + " and next: " + m_nextQueue.size());
                        SrcClusterStatsKryo waterMarkEntry = new SrcClusterStatsKryo();
                        waterMarkEntry.m_src_cluster = -1;
//                        m_nextQueue.offer(waterMarkEntry);
                        m_kryo.writeObject(m_output, waterMarkEntry);
                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }

                    @Override
                    public void onNext(SrcClusterStatsKryo srcClusterStatsKryo) {
                        try {
//                            m_nextQueue.offer(srcClusterStatsKryo);
                            m_kryo.writeObject(m_output, srcClusterStatsKryo);
                            m_numOutRecords[0]++;
                        } catch (Exception e) {
                            m_logger.debug("Couldn't write to output stream : " + e.toString());
                        }
                    }
                }
        );;


//        m_logger.debug("Agg Dataflow build duration is " + (System.currentTimeMillis() - startDataflowBuild));
        m_dataflowBuildDur=(System.currentTimeMillis() - startDataflowBuild);
    }

    public void setNextQueue(LinkedBlockingQueue<SrcClusterStatsKryo> nextQueue) {
        m_nextQueue = nextQueue;
    }

    public void setKryoSerializer(Kryo kryo) {
        m_kryo = kryo;
    }

    public void setOutput(Output output) {
        m_output = output;
    }

    public Integer[] getOutputRecCount() {
        return m_numOutRecords;
    }

    public void setCurrentQueue(LinkedBlockingQueue<GrpedObsWithTime> currentQueue) {
        m_currentQueue = currentQueue;
    }

    public void setSubject(io.reactivex.subjects.PublishSubject<io.reactivex.observables.GroupedObservable<Integer, PingMeshKryo>> subject) {
        m_subject = subject;
    }

    public void setObserver(io.reactivex.subjects.PublishSubject<PingMeshKryoWithTime> subject) {
//        m_dataflow = io.reactivex.Observable.just(m_currentQueue).flatMapIterable(elements -> elements).

//                io.reactivex.Observable.fromIterable(m_currentQueue).
//                    doOnNext(System.out::println).
//                    filter(v -> v.m_errcode==1);
//        m_dataflow = m_subject.doOnNext(m_currentQueue::remove);
//        m_dataflow = subject.filter(v -> v.m_errcode==1);
    }

    public void run() {
//        m_dataflow = Observable.just(m_currentQueue.poll()).
//                observe().
        GrpedObsWithTime readObj;
        long totalQueueTime = 0;
        int recordCount = 0;
        try {
//            m_logger.debug("Dequeue start agg: " + System.currentTimeMillis());
            while ((readObj = m_currentQueue.take()) != null) {
                if(readObj.m_watermark) {
                    m_subject.onComplete();
                    break;
                }
//                System.out.println("Record");
                totalQueueTime = readObj.getQueueTime();
                m_subject.onNext(readObj.m_data);
                recordCount++;
//                System.out.println("[Agg] Queue size is " + m_currentQueue.size()
//                    + " and queue time is " + totalQueueTime);
                if(m_maxQueueTime < totalQueueTime) {
                    m_maxQueueTime = totalQueueTime;
                }
//                if(recordCount == m_flowFileSize) {
//                    m_subject.onComplete();
//                    break;
//                }
            }

//            m_logger.debug("Agg max queue time = " + maxQueueTime);
//            m_logger.debug("Agg avg processing time in ns = " + (totalProcessingTime/recordCount));
            m_avgProcessingTime=(float)totalProcessingTime/(float)recordCount;
        } catch (Exception ex) {
            m_logger.debug("Exception waiting: " + ex.toString());
        }
    }
}

