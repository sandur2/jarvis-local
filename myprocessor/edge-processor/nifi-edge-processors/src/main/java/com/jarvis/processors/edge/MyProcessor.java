/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jarvis.processors.edge;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.io.InputStreamCallback;
import org.apache.nifi.processor.util.StandardValidators;
import org.apache.nifi.util.NiFiProperties;
import org.omg.SendingContext.RunTime;

import javax.naming.ldap.Control;
import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Tags({"example"})
@CapabilityDescription("Provide a description")
@SeeAlso({})
@ReadsAttributes({@ReadsAttribute(attribute="", description="")})
@WritesAttributes({@WritesAttribute(attribute="", description="")})
public class MyProcessor extends AbstractSessionFactoryProcessor {

    public static final PropertyDescriptor MY_PROPERTY = new PropertyDescriptor
            .Builder().name("MY_PROPERTY")
            .displayName("My property")
            .description("Example Property")
            .required(true)
            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .build();

    public static final Relationship MY_RELATIONSHIP = new Relationship.Builder()
            .name("MY_RELATIONSHIP")
            .description("Example relationship")
            .build();

    public static final Relationship MY_RELATIONSHIP_1 = new Relationship.Builder()
            .name("MY_RELATIONSHIP_1")
            .description("Example relationship 1")
            .build();

    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    // Custom Jarvis code
//    CustomOperator mapOp;
    ControllerQueue m_operatorQueue;
    ControllerQueue m_operatorQueue1;
    OutputQueueWrapper m_exitQueue;
    PingMeshKryoWithTimePool m_pool;
    NiFiProperties m_nifiProperties;
    Kryo m_kryo;
    int m_numOperators;
    Thread[] m_mapThread = new Thread[2];
    Thread runtimeThread = new Thread();
    CloudUploader m_cloudUploader;
    public Object m_waterMarkInExitLock = new Object();
    public boolean m_watermarkObserved = false;

    public static Object m_sessionFactoryLock;
    public static ProcessSessionFactory m_sessionFactory;

    public AtomicInteger m_waterMarkSeqNum = new AtomicInteger(0);

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
//        descriptors.add(MY_PROPERTY);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(MY_RELATIONSHIP);
        relationships.add(MY_RELATIONSHIP_1);
        this.relationships = Collections.unmodifiableSet(relationships);

        // Custom Jarvis code

        PingMeshKryo dummyWaterMark = new PingMeshKryo();
        dummyWaterMark.m_constantCol= Integer.MIN_VALUE;
        PingMeshKryoWithTime dummyWaterMarkWithTime = new PingMeshKryoWithTime();
        dummyWaterMarkWithTime.setEntity(dummyWaterMark);

        JarvisLogger.initialize(getLogger());
        m_kryo = new Kryo();
        m_kryo.register(PingMeshKryo.class);
        m_kryo.register(PingMeshKryoWithTime.class);
        m_kryo.register(Integer.class);
        m_kryo.register(SrcClusterStatsKryo.class);
        String queueThresholdConfig = "15000000";
        JarvisLogger.info("[Atul] Queue threshold config is " + queueThresholdConfig);
        m_operatorQueue = new ControllerQueue(1, Integer.parseInt(queueThresholdConfig), m_kryo,
                MY_RELATIONSHIP_1, dummyWaterMarkWithTime);
        m_operatorQueue1 = new ControllerQueue(2, Integer.parseInt(queueThresholdConfig), m_kryo,
                MY_RELATIONSHIP_1, dummyWaterMarkWithTime);
//        m_operatorQueue.initializeKryo(new PingMeshKryoWithTime(), new PingMeshKryo());
        m_exitQueue = new OutputQueueWrapper(m_kryo, MY_RELATIONSHIP);
//        m_exitQueue.initializeKryo(new PingMeshKryo());
        m_pool = new PingMeshKryoWithTimePool(148545);
//        m_nifiProperties = new NiFiProperties() {
//            @Override
//            public String getProperty(String s) {
//                return NiFiProperties.VARIABLE_REGISTRY_PROPERTIES;
//            }
//
//            @Override
//            public Set<String> getPropertyKeys() {
//                return null;
//            }
//        };

//        String queueThresholdConfig = m_nifiProperties.getProperty(null);
//        String queueThresholdConfig = "150000";
        m_numOperators = 2;
        m_sessionFactoryLock = new Object();
        CustomMapOperator mapOp = new CustomMapOperator(1, m_operatorQueue, 1);
        mapOp.setNextQueue(m_operatorQueue1);
        CustomMapOperator mapOp1 = new CustomMapOperator(2, m_operatorQueue1, 0.4);
        mapOp1.setNextQueue(m_exitQueue);
        m_mapThread[0] = new Thread(mapOp);
        m_mapThread[0].start();
        m_mapThread[1] = new Thread(mapOp1);
        m_mapThread[1].start();

        // Start runtime in the end
        Runtime jarvisRuntime = new Runtime(m_exitQueue, m_operatorQueue, m_operatorQueue1);
        runtimeThread = new Thread(jarvisRuntime);
        runtimeThread.start();

//        try {
//            mapThread[0].join();
////            mapOp.run();
//        } catch (Exception ex){
//            this.m_logger.debug("[MY DEBUG] Joining on grp and agg threads failed");
//        }

    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }

    @OnScheduled
    public void onScheduled(final ProcessContext context) {

    }

    @Override
    public final void onTrigger(ProcessContext context, ProcessSessionFactory sessionFactory) throws ProcessException {
        ProcessSession session;
        synchronized (m_sessionFactoryLock) {
            m_sessionFactory = sessionFactory;
            session = m_sessionFactory.createSession();
        }

        try {
            this.extractRecords(context, session);
            session.commit();
        } catch (Throwable var5) {
            session.rollback(true);
            throw var5;
        }
    }

    public void extractRecords(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();

        // To get multiple flowfiles
        // session.get(context.getProperty("BATCH_SIZE").asInteger());
        if ( flowFile == null ) {
            return;
        }

        // TODO implement

        // Write current timestamp to get queueing time
        long queueEndTime = System.currentTimeMillis();

        // Profiling operators
        long[] startExtract = {0};
        long[] endExtract = {0};

        Input[] input = new Input[1];

        session.read(flowFile, new InputStreamCallback() {
            @Override
            public void process(InputStream inputStream) throws IOException {
                try{
                    System.setProperty("rx.ring-buffer.size", "150000");

                    PingMeshKryo waterMark = new PingMeshKryo();
                    waterMark.m_constantCol= Integer.MIN_VALUE;
                    PingMeshKryoWithTime waterMarkWithTime = new PingMeshKryoWithTime();
                    waterMarkWithTime.setEntity(waterMark);

                    // Reading the data
                    input[0] = new Input(inputStream);
                    startExtract[0] = System.currentTimeMillis();
                    Integer numRecords = (Integer) m_kryo.readObject(input[0], Integer.class);
                    PingMeshKryo object2 = m_kryo.readObject(input[0], PingMeshKryo.class);
                    m_pool.pingMeshKryoWithTimes[0].setEntity(object2);
                    m_operatorQueue.put(m_pool.pingMeshKryoWithTimes[0]);
                    int read = 1;
                    while(read < numRecords) {
                        object2 = m_kryo.readObject(input[0], PingMeshKryo.class);
                        m_pool.pingMeshKryoWithTimes[read].setEntity(object2);
                        m_operatorQueue.put(m_pool.pingMeshKryoWithTimes[read]);
                        read++;
                    }

                    endExtract[0] = System.currentTimeMillis();
                    waterMarkWithTime.setSeqNum(m_waterMarkSeqNum.getAndIncrement());
                    m_operatorQueue.putWaterMark(waterMarkWithTime);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("File not found exception");
                }

            }
        });

        // Close the input stream before joining on the thread
        input[0].close();

        session.remove(flowFile);

    }
}
