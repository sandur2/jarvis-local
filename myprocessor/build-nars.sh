#!/bin/bash

cd /home/athuls89/Desktop/OSL/msr/calcite/myprocessor/cloud-processor
mvn clean install
scp nifi-cloud-nar/target/nifi-cloud-nar-1.0-SNAPSHOT.nar athuls89@osl-server1.cs.illinois.edu:/home/athuls89/Desktop/OSL/msr/nifi/edge-cloud-analytics/nifi-1.7.0/lib
scp nifi-cloud-nar/target/nifi-cloud-nar-1.0-SNAPSHOT.nar athuls89@osl-server1.cs.illinois.edu:/home/athuls89/Desktop/OSL/msr/minifi/jarvis-minifi/minifi/minifi-0.5.0/lib

cd /home/athuls89/Desktop/OSL/msr/calcite/myprocessor/edge-processor
mvn clean install
scp nifi-edge-nar/target/nifi-edge-nar-1.0-SNAPSHOT.nar athuls89@osl-server1.cs.illinois.edu:/home/athuls89/Desktop/OSL/msr/nifi/edge-cloud-analytics/nifi-1.7.0/lib
scp nifi-edge-nar/target/nifi-edge-nar-1.0-SNAPSHOT.nar athuls89@osl-server1.cs.illinois.edu:/home/athuls89/Desktop/OSL/msr/minifi/jarvis-minifi/minifi/minifi-0.5.0/lib
